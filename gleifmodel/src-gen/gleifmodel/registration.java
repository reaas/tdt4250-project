/**
 */
package gleifmodel;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>registration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.registration#getInitialRegistrationDate <em>Initial Registration Date</em>}</li>
 *   <li>{@link gleifmodel.registration#getLastUpdateDate <em>Last Update Date</em>}</li>
 *   <li>{@link gleifmodel.registration#getStatus <em>Status</em>}</li>
 *   <li>{@link gleifmodel.registration#getNextRenewalDate <em>Next Renewal Date</em>}</li>
 * </ul>
 *
 * @see gleifmodel.GleifmodelPackage#getregistration()
 * @model
 * @generated
 */
public interface registration extends EObject {
	/**
	 * Returns the value of the '<em><b>Initial Registration Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Registration Date</em>' attribute.
	 * @see #setInitialRegistrationDate(Date)
	 * @see gleifmodel.GleifmodelPackage#getregistration_InitialRegistrationDate()
	 * @model
	 * @generated
	 */
	Date getInitialRegistrationDate();

	/**
	 * Sets the value of the '{@link gleifmodel.registration#getInitialRegistrationDate <em>Initial Registration Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Registration Date</em>' attribute.
	 * @see #getInitialRegistrationDate()
	 * @generated
	 */
	void setInitialRegistrationDate(Date value);

	/**
	 * Returns the value of the '<em><b>Last Update Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Update Date</em>' attribute.
	 * @see #setLastUpdateDate(Date)
	 * @see gleifmodel.GleifmodelPackage#getregistration_LastUpdateDate()
	 * @model
	 * @generated
	 */
	Date getLastUpdateDate();

	/**
	 * Sets the value of the '{@link gleifmodel.registration#getLastUpdateDate <em>Last Update Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Update Date</em>' attribute.
	 * @see #getLastUpdateDate()
	 * @generated
	 */
	void setLastUpdateDate(Date value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link gleifmodel.registartionStatus}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see gleifmodel.registartionStatus
	 * @see #setStatus(registartionStatus)
	 * @see gleifmodel.GleifmodelPackage#getregistration_Status()
	 * @model
	 * @generated
	 */
	registartionStatus getStatus();

	/**
	 * Sets the value of the '{@link gleifmodel.registration#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see gleifmodel.registartionStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(registartionStatus value);

	/**
	 * Returns the value of the '<em><b>Next Renewal Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Renewal Date</em>' attribute.
	 * @see #setNextRenewalDate(Date)
	 * @see gleifmodel.GleifmodelPackage#getregistration_NextRenewalDate()
	 * @model
	 * @generated
	 */
	Date getNextRenewalDate();

	/**
	 * Sets the value of the '{@link gleifmodel.registration#getNextRenewalDate <em>Next Renewal Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Renewal Date</em>' attribute.
	 * @see #getNextRenewalDate()
	 * @generated
	 */
	void setNextRenewalDate(Date value);

} // registration
