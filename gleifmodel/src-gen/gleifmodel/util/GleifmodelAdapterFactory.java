/**
 */
package gleifmodel.util;

import gleifmodel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see gleifmodel.GleifmodelPackage
 * @generated
 */
public class GleifmodelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GleifmodelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GleifmodelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = GleifmodelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GleifmodelSwitch<Adapter> modelSwitch = new GleifmodelSwitch<Adapter>() {
		@Override
		public Adapter caselegalName(legalName object) {
			return createlegalNameAdapter();
		}

		@Override
		public Adapter caseaddress(address object) {
			return createaddressAdapter();
		}

		@Override
		public Adapter caserelatedEntity(relatedEntity object) {
			return createrelatedEntityAdapter();
		}

		@Override
		public Adapter casecompany(company object) {
			return createcompanyAdapter();
		}

		@Override
		public Adapter caseregistration(registration object) {
			return createregistrationAdapter();
		}

		@Override
		public Adapter caseleiRecord(leiRecord object) {
			return createleiRecordAdapter();
		}

		@Override
		public Adapter caserelationships(relationships object) {
			return createrelationshipsAdapter();
		}

		@Override
		public Adapter caselinks(links object) {
			return createlinksAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.legalName <em>legal Name</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.legalName
	 * @generated
	 */
	public Adapter createlegalNameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.address <em>address</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.address
	 * @generated
	 */
	public Adapter createaddressAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.relatedEntity <em>related Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.relatedEntity
	 * @generated
	 */
	public Adapter createrelatedEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.company <em>company</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.company
	 * @generated
	 */
	public Adapter createcompanyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.registration <em>registration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.registration
	 * @generated
	 */
	public Adapter createregistrationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.leiRecord <em>lei Record</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.leiRecord
	 * @generated
	 */
	public Adapter createleiRecordAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.relationships <em>relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.relationships
	 * @generated
	 */
	public Adapter createrelationshipsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gleifmodel.links <em>links</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gleifmodel.links
	 * @generated
	 */
	public Adapter createlinksAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //GleifmodelAdapterFactory
