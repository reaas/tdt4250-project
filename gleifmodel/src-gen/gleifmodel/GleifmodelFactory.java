/**
 */
package gleifmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gleifmodel.GleifmodelPackage
 * @generated
 */
public interface GleifmodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GleifmodelFactory eINSTANCE = gleifmodel.impl.GleifmodelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>legal Name</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>legal Name</em>'.
	 * @generated
	 */
	legalName createlegalName();

	/**
	 * Returns a new object of class '<em>address</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>address</em>'.
	 * @generated
	 */
	address createaddress();

	/**
	 * Returns a new object of class '<em>related Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>related Entity</em>'.
	 * @generated
	 */
	relatedEntity createrelatedEntity();

	/**
	 * Returns a new object of class '<em>company</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>company</em>'.
	 * @generated
	 */
	company createcompany();

	/**
	 * Returns a new object of class '<em>registration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>registration</em>'.
	 * @generated
	 */
	registration createregistration();

	/**
	 * Returns a new object of class '<em>lei Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>lei Record</em>'.
	 * @generated
	 */
	leiRecord createleiRecord();

	/**
	 * Returns a new object of class '<em>relationships</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>relationships</em>'.
	 * @generated
	 */
	relationships createrelationships();

	/**
	 * Returns a new object of class '<em>links</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>links</em>'.
	 * @generated
	 */
	links createlinks();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GleifmodelPackage getGleifmodelPackage();

} //GleifmodelFactory
