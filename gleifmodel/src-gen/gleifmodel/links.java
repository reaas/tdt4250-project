/**
 */
package gleifmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>links</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.links#getLeiRecord <em>Lei Record</em>}</li>
 *   <li>{@link gleifmodel.links#getRelationshipRecord <em>Relationship Record</em>}</li>
 * </ul>
 *
 * @see gleifmodel.GleifmodelPackage#getlinks()
 * @model
 * @generated
 */
public interface links extends EObject {
	/**
	 * Returns the value of the '<em><b>Lei Record</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lei Record</em>' reference.
	 * @see #setLeiRecord(leiRecord)
	 * @see gleifmodel.GleifmodelPackage#getlinks_LeiRecord()
	 * @model
	 * @generated
	 */
	leiRecord getLeiRecord();

	/**
	 * Sets the value of the '{@link gleifmodel.links#getLeiRecord <em>Lei Record</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lei Record</em>' reference.
	 * @see #getLeiRecord()
	 * @generated
	 */
	void setLeiRecord(leiRecord value);

	/**
	 * Returns the value of the '<em><b>Relationship Record</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Record</em>' reference.
	 * @see #setRelationshipRecord(leiRecord)
	 * @see gleifmodel.GleifmodelPackage#getlinks_RelationshipRecord()
	 * @model
	 * @generated
	 */
	leiRecord getRelationshipRecord();

	/**
	 * Sets the value of the '{@link gleifmodel.links#getRelationshipRecord <em>Relationship Record</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationship Record</em>' reference.
	 * @see #getRelationshipRecord()
	 * @generated
	 */
	void setRelationshipRecord(leiRecord value);

} // links
