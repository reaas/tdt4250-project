/**
 */
package gleifmodel.gleifmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>relationships</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.gleifmodel.relationships#getManagingLou <em>Managing Lou</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.relationships#getLeiIssuer <em>Lei Issuer</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.relationships#getDirectParent <em>Direct Parent</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.relationships#getUltimateParent <em>Ultimate Parent</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.relationships#getDirectChildren <em>Direct Children</em>}</li>
 * </ul>
 *
 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelationships()
 * @model
 * @generated
 */
public interface relationships extends EObject {
	/**
	 * Returns the value of the '<em><b>Managing Lou</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Managing Lou</em>' reference.
	 * @see #setManagingLou(links)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelationships_ManagingLou()
	 * @model
	 * @generated
	 */
	links getManagingLou();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relationships#getManagingLou <em>Managing Lou</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Managing Lou</em>' reference.
	 * @see #getManagingLou()
	 * @generated
	 */
	void setManagingLou(links value);

	/**
	 * Returns the value of the '<em><b>Lei Issuer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lei Issuer</em>' reference.
	 * @see #setLeiIssuer(links)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelationships_LeiIssuer()
	 * @model
	 * @generated
	 */
	links getLeiIssuer();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relationships#getLeiIssuer <em>Lei Issuer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lei Issuer</em>' reference.
	 * @see #getLeiIssuer()
	 * @generated
	 */
	void setLeiIssuer(links value);

	/**
	 * Returns the value of the '<em><b>Direct Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Parent</em>' reference.
	 * @see #setDirectParent(links)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelationships_DirectParent()
	 * @model
	 * @generated
	 */
	links getDirectParent();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relationships#getDirectParent <em>Direct Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direct Parent</em>' reference.
	 * @see #getDirectParent()
	 * @generated
	 */
	void setDirectParent(links value);

	/**
	 * Returns the value of the '<em><b>Ultimate Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ultimate Parent</em>' reference.
	 * @see #setUltimateParent(links)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelationships_UltimateParent()
	 * @model
	 * @generated
	 */
	links getUltimateParent();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relationships#getUltimateParent <em>Ultimate Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ultimate Parent</em>' reference.
	 * @see #getUltimateParent()
	 * @generated
	 */
	void setUltimateParent(links value);

	/**
	 * Returns the value of the '<em><b>Direct Children</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Children</em>' reference.
	 * @see #setDirectChildren(links)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelationships_DirectChildren()
	 * @model
	 * @generated
	 */
	links getDirectChildren();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relationships#getDirectChildren <em>Direct Children</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direct Children</em>' reference.
	 * @see #getDirectChildren()
	 * @generated
	 */
	void setDirectChildren(links value);

} // relationships
