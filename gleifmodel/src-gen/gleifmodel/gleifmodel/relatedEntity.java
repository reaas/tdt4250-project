/**
 */
package gleifmodel.gleifmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>related Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.gleifmodel.relatedEntity#getLei <em>Lei</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.relatedEntity#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelatedEntity()
 * @model
 * @generated
 */
public interface relatedEntity extends EObject {
	/**
	 * Returns the value of the '<em><b>Lei</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lei</em>' attribute.
	 * @see #setLei(String)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelatedEntity_Lei()
	 * @model
	 * @generated
	 */
	String getLei();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relatedEntity#getLei <em>Lei</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lei</em>' attribute.
	 * @see #getLei()
	 * @generated
	 */
	void setLei(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getrelatedEntity_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.relatedEntity#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // relatedEntity
