/**
 */
package gleifmodel.gleifmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>lei Record</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.gleifmodel.leiRecord#getLei <em>Lei</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.leiRecord#getEntity <em>Entity</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.leiRecord#getRegistration <em>Registration</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.leiRecord#getLinks <em>Links</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.leiRecord#getRelationships <em>Relationships</em>}</li>
 * </ul>
 *
 * @see gleifmodel.gleifmodel.GleifmodelPackage#getleiRecord()
 * @model
 * @generated
 */
public interface leiRecord extends EObject {
	/**
	 * Returns the value of the '<em><b>Lei</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lei</em>' attribute.
	 * @see #setLei(String)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getleiRecord_Lei()
	 * @model
	 * @generated
	 */
	String getLei();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.leiRecord#getLei <em>Lei</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lei</em>' attribute.
	 * @see #getLei()
	 * @generated
	 */
	void setLei(String value);

	/**
	 * Returns the value of the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' reference.
	 * @see #setEntity(company)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getleiRecord_Entity()
	 * @model
	 * @generated
	 */
	company getEntity();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.leiRecord#getEntity <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(company value);

	/**
	 * Returns the value of the '<em><b>Registration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registration</em>' reference.
	 * @see #setRegistration(registration)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getleiRecord_Registration()
	 * @model
	 * @generated
	 */
	registration getRegistration();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.leiRecord#getRegistration <em>Registration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Registration</em>' reference.
	 * @see #getRegistration()
	 * @generated
	 */
	void setRegistration(registration value);

	/**
	 * Returns the value of the '<em><b>Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links</em>' reference.
	 * @see #setLinks(links)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getleiRecord_Links()
	 * @model
	 * @generated
	 */
	links getLinks();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.leiRecord#getLinks <em>Links</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Links</em>' reference.
	 * @see #getLinks()
	 * @generated
	 */
	void setLinks(links value);

	/**
	 * Returns the value of the '<em><b>Relationships</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationships</em>' reference.
	 * @see #setRelationships(relationships)
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getleiRecord_Relationships()
	 * @model
	 * @generated
	 */
	relationships getRelationships();

	/**
	 * Sets the value of the '{@link gleifmodel.gleifmodel.leiRecord#getRelationships <em>Relationships</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationships</em>' reference.
	 * @see #getRelationships()
	 * @generated
	 */
	void setRelationships(relationships value);

} // leiRecord
