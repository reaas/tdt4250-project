/**
 */
package gleifmodel.gleifmodel.impl;

import gleifmodel.gleifmodel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GleifmodelFactoryImpl extends EFactoryImpl implements GleifmodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GleifmodelFactory init() {
		try {
			GleifmodelFactory theGleifmodelFactory = (GleifmodelFactory) EPackage.Registry.INSTANCE
					.getEFactory(GleifmodelPackage.eNS_URI);
			if (theGleifmodelFactory != null) {
				return theGleifmodelFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GleifmodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GleifmodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case GleifmodelPackage.LEGAL_NAME:
			return createlegalName();
		case GleifmodelPackage.ADDRESS:
			return createaddress();
		case GleifmodelPackage.RELATED_ENTITY:
			return createrelatedEntity();
		case GleifmodelPackage.COMPANY:
			return createcompany();
		case GleifmodelPackage.REGISTRATION:
			return createregistration();
		case GleifmodelPackage.LEI_RECORD:
			return createleiRecord();
		case GleifmodelPackage.RELATIONSHIPS:
			return createrelationships();
		case GleifmodelPackage.LINKS:
			return createlinks();
		case GleifmodelPackage.GLEIF:
			return creategleif();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case GleifmodelPackage.ENTITY_STATUS:
			return createentityStatusFromString(eDataType, initialValue);
		case GleifmodelPackage.REGISTARTION_STATUS:
			return createregistartionStatusFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case GleifmodelPackage.ENTITY_STATUS:
			return convertentityStatusToString(eDataType, instanceValue);
		case GleifmodelPackage.REGISTARTION_STATUS:
			return convertregistartionStatusToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public legalName createlegalName() {
		legalNameImpl legalName = new legalNameImpl();
		return legalName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public address createaddress() {
		addressImpl address = new addressImpl();
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public relatedEntity createrelatedEntity() {
		relatedEntityImpl relatedEntity = new relatedEntityImpl();
		return relatedEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public company createcompany() {
		companyImpl company = new companyImpl();
		return company;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public registration createregistration() {
		registrationImpl registration = new registrationImpl();
		return registration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public leiRecord createleiRecord() {
		leiRecordImpl leiRecord = new leiRecordImpl();
		return leiRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public relationships createrelationships() {
		relationshipsImpl relationships = new relationshipsImpl();
		return relationships;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links createlinks() {
		linksImpl links = new linksImpl();
		return links;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public gleif creategleif() {
		gleifImpl gleif = new gleifImpl();
		return gleif;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public entityStatus createentityStatusFromString(EDataType eDataType, String initialValue) {
		entityStatus result = entityStatus.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertentityStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public registartionStatus createregistartionStatusFromString(EDataType eDataType, String initialValue) {
		registartionStatus result = registartionStatus.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertregistartionStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GleifmodelPackage getGleifmodelPackage() {
		return (GleifmodelPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GleifmodelPackage getPackage() {
		return GleifmodelPackage.eINSTANCE;
	}

} //GleifmodelFactoryImpl
