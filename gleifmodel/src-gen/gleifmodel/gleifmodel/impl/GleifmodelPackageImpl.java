/**
 */
package gleifmodel.gleifmodel.impl;

import gleifmodel.gleifmodel.GleifmodelFactory;
import gleifmodel.gleifmodel.GleifmodelPackage;
import gleifmodel.gleifmodel.address;
import gleifmodel.gleifmodel.company;
import gleifmodel.gleifmodel.entityStatus;
import gleifmodel.gleifmodel.gleif;
import gleifmodel.gleifmodel.legalName;
import gleifmodel.gleifmodel.leiRecord;
import gleifmodel.gleifmodel.links;
import gleifmodel.gleifmodel.registartionStatus;
import gleifmodel.gleifmodel.registration;
import gleifmodel.gleifmodel.relatedEntity;
import gleifmodel.gleifmodel.relationships;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GleifmodelPackageImpl extends EPackageImpl implements GleifmodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass legalNameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addressEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relatedEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass registrationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leiRecordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gleifEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum entityStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum registartionStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GleifmodelPackageImpl() {
		super(eNS_URI, GleifmodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link GleifmodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GleifmodelPackage init() {
		if (isInited)
			return (GleifmodelPackage) EPackage.Registry.INSTANCE.getEPackage(GleifmodelPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredGleifmodelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		GleifmodelPackageImpl theGleifmodelPackage = registeredGleifmodelPackage instanceof GleifmodelPackageImpl
				? (GleifmodelPackageImpl) registeredGleifmodelPackage
				: new GleifmodelPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theGleifmodelPackage.createPackageContents();

		// Initialize created meta-data
		theGleifmodelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGleifmodelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GleifmodelPackage.eNS_URI, theGleifmodelPackage);
		return theGleifmodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getlegalName() {
		return legalNameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getlegalName_Name() {
		return (EAttribute) legalNameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getlegalName_Language() {
		return (EAttribute) legalNameEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getaddress() {
		return addressEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_Language() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_AddressLines() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_AddressNumber() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_AddressNumberWithinBuilding() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_MailRouting() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_City() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_Region() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_Country() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getaddress_PostalCode() {
		return (EAttribute) addressEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getrelatedEntity() {
		return relatedEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getrelatedEntity_Lei() {
		return (EAttribute) relatedEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getrelatedEntity_Name() {
		return (EAttribute) relatedEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getcompany() {
		return companyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getcompany_LegalName() {
		return (EReference) companyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getcompany_LegalAddress() {
		return (EReference) companyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getcompany_HeadquartersAddress() {
		return (EReference) companyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getcompany_RegisteredAs() {
		return (EAttribute) companyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getcompany_Jurisdiction() {
		return (EAttribute) companyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getcompany_AssociatedEntity() {
		return (EReference) companyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getcompany_Status() {
		return (EAttribute) companyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getcompany_SuccessorEntity() {
		return (EReference) companyEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getregistration() {
		return registrationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getregistration_InitialRegistrationDate() {
		return (EAttribute) registrationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getregistration_LastUpdateDate() {
		return (EAttribute) registrationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getregistration_Status() {
		return (EAttribute) registrationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getregistration_NextRenewalDate() {
		return (EAttribute) registrationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getleiRecord() {
		return leiRecordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getleiRecord_Lei() {
		return (EAttribute) leiRecordEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getleiRecord_Entity() {
		return (EReference) leiRecordEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getleiRecord_Registration() {
		return (EReference) leiRecordEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getleiRecord_Links() {
		return (EReference) leiRecordEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getleiRecord_Relationships() {
		return (EReference) leiRecordEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getrelationships() {
		return relationshipsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getrelationships_ManagingLou() {
		return (EReference) relationshipsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getrelationships_LeiIssuer() {
		return (EReference) relationshipsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getrelationships_DirectParent() {
		return (EReference) relationshipsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getrelationships_UltimateParent() {
		return (EReference) relationshipsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getrelationships_DirectChildren() {
		return (EReference) relationshipsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getlinks() {
		return linksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getlinks_LeiRecord() {
		return (EReference) linksEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getlinks_RelationshipRecord() {
		return (EReference) linksEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getgleif() {
		return gleifEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getgleif_Records() {
		return (EReference) gleifEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getentityStatus() {
		return entityStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getregistartionStatus() {
		return registartionStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GleifmodelFactory getGleifmodelFactory() {
		return (GleifmodelFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		legalNameEClass = createEClass(LEGAL_NAME);
		createEAttribute(legalNameEClass, LEGAL_NAME__NAME);
		createEAttribute(legalNameEClass, LEGAL_NAME__LANGUAGE);

		addressEClass = createEClass(ADDRESS);
		createEAttribute(addressEClass, ADDRESS__LANGUAGE);
		createEAttribute(addressEClass, ADDRESS__ADDRESS_LINES);
		createEAttribute(addressEClass, ADDRESS__ADDRESS_NUMBER);
		createEAttribute(addressEClass, ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING);
		createEAttribute(addressEClass, ADDRESS__MAIL_ROUTING);
		createEAttribute(addressEClass, ADDRESS__CITY);
		createEAttribute(addressEClass, ADDRESS__REGION);
		createEAttribute(addressEClass, ADDRESS__COUNTRY);
		createEAttribute(addressEClass, ADDRESS__POSTAL_CODE);

		relatedEntityEClass = createEClass(RELATED_ENTITY);
		createEAttribute(relatedEntityEClass, RELATED_ENTITY__LEI);
		createEAttribute(relatedEntityEClass, RELATED_ENTITY__NAME);

		companyEClass = createEClass(COMPANY);
		createEReference(companyEClass, COMPANY__LEGAL_NAME);
		createEReference(companyEClass, COMPANY__LEGAL_ADDRESS);
		createEReference(companyEClass, COMPANY__HEADQUARTERS_ADDRESS);
		createEAttribute(companyEClass, COMPANY__REGISTERED_AS);
		createEAttribute(companyEClass, COMPANY__JURISDICTION);
		createEReference(companyEClass, COMPANY__ASSOCIATED_ENTITY);
		createEAttribute(companyEClass, COMPANY__STATUS);
		createEReference(companyEClass, COMPANY__SUCCESSOR_ENTITY);

		registrationEClass = createEClass(REGISTRATION);
		createEAttribute(registrationEClass, REGISTRATION__INITIAL_REGISTRATION_DATE);
		createEAttribute(registrationEClass, REGISTRATION__LAST_UPDATE_DATE);
		createEAttribute(registrationEClass, REGISTRATION__STATUS);
		createEAttribute(registrationEClass, REGISTRATION__NEXT_RENEWAL_DATE);

		leiRecordEClass = createEClass(LEI_RECORD);
		createEAttribute(leiRecordEClass, LEI_RECORD__LEI);
		createEReference(leiRecordEClass, LEI_RECORD__ENTITY);
		createEReference(leiRecordEClass, LEI_RECORD__REGISTRATION);
		createEReference(leiRecordEClass, LEI_RECORD__LINKS);
		createEReference(leiRecordEClass, LEI_RECORD__RELATIONSHIPS);

		relationshipsEClass = createEClass(RELATIONSHIPS);
		createEReference(relationshipsEClass, RELATIONSHIPS__MANAGING_LOU);
		createEReference(relationshipsEClass, RELATIONSHIPS__LEI_ISSUER);
		createEReference(relationshipsEClass, RELATIONSHIPS__DIRECT_PARENT);
		createEReference(relationshipsEClass, RELATIONSHIPS__ULTIMATE_PARENT);
		createEReference(relationshipsEClass, RELATIONSHIPS__DIRECT_CHILDREN);

		linksEClass = createEClass(LINKS);
		createEReference(linksEClass, LINKS__LEI_RECORD);
		createEReference(linksEClass, LINKS__RELATIONSHIP_RECORD);

		gleifEClass = createEClass(GLEIF);
		createEReference(gleifEClass, GLEIF__RECORDS);

		// Create enums
		entityStatusEEnum = createEEnum(ENTITY_STATUS);
		registartionStatusEEnum = createEEnum(REGISTARTION_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(legalNameEClass, legalName.class, "legalName", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getlegalName_Name(), ecorePackage.getEString(), "name", null, 0, 1, legalName.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getlegalName_Language(), ecorePackage.getEString(), "language", null, 0, 1, legalName.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(addressEClass, address.class, "address", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getaddress_Language(), ecorePackage.getEString(), "language", null, 0, 1, address.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEEList());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getaddress_AddressLines(), g1, "addressLines", null, 0, 1, address.class, IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_AddressNumber(), ecorePackage.getEString(), "addressNumber", null, 0, 1,
				address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_AddressNumberWithinBuilding(), ecorePackage.getEString(),
				"addressNumberWithinBuilding", null, 0, 1, address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_MailRouting(), ecorePackage.getEString(), "mailRouting", null, 0, 1, address.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_City(), ecorePackage.getEString(), "city", null, 0, 1, address.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_Region(), ecorePackage.getEString(), "region", null, 0, 1, address.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_Country(), ecorePackage.getEString(), "country", null, 0, 1, address.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getaddress_PostalCode(), ecorePackage.getEString(), "postalCode", null, 0, 1, address.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relatedEntityEClass, relatedEntity.class, "relatedEntity", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getrelatedEntity_Lei(), ecorePackage.getEString(), "lei", null, 0, 1, relatedEntity.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getrelatedEntity_Name(), ecorePackage.getEString(), "name", null, 0, 1, relatedEntity.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(companyEClass, company.class, "company", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getcompany_LegalName(), this.getlegalName(), null, "legalName", null, 0, 1, company.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getcompany_LegalAddress(), this.getaddress(), null, "legalAddress", null, 0, 1, company.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getcompany_HeadquartersAddress(), this.getaddress(), null, "headquartersAddress", null, 0, 1,
				company.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getcompany_RegisteredAs(), ecorePackage.getEString(), "registeredAs", null, 0, 1, company.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getcompany_Jurisdiction(), ecorePackage.getEString(), "jurisdiction", null, 0, 1, company.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getcompany_AssociatedEntity(), this.getrelatedEntity(), null, "associatedEntity", null, 0, 1,
				company.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getcompany_Status(), this.getentityStatus(), "status", null, 0, 1, company.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getcompany_SuccessorEntity(), this.getrelatedEntity(), null, "successorEntity", null, 0, 1,
				company.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(registrationEClass, registration.class, "registration", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getregistration_InitialRegistrationDate(), ecorePackage.getEDate(), "initialRegistrationDate",
				null, 0, 1, registration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getregistration_LastUpdateDate(), ecorePackage.getEDate(), "lastUpdateDate", null, 0, 1,
				registration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getregistration_Status(), this.getregistartionStatus(), "status", null, 0, 1, registration.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getregistration_NextRenewalDate(), ecorePackage.getEDate(), "nextRenewalDate", null, 0, 1,
				registration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(leiRecordEClass, leiRecord.class, "leiRecord", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getleiRecord_Lei(), ecorePackage.getEString(), "lei", null, 0, 1, leiRecord.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getleiRecord_Entity(), this.getcompany(), null, "entity", null, 0, 1, leiRecord.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getleiRecord_Registration(), this.getregistration(), null, "registration", null, 0, 1,
				leiRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getleiRecord_Links(), this.getlinks(), null, "links", null, 0, 1, leiRecord.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getleiRecord_Relationships(), this.getrelationships(), null, "relationships", null, 0, 1,
				leiRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationshipsEClass, relationships.class, "relationships", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getrelationships_ManagingLou(), this.getlinks(), null, "managingLou", null, 0, 1,
				relationships.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getrelationships_LeiIssuer(), this.getlinks(), null, "leiIssuer", null, 0, 1,
				relationships.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getrelationships_DirectParent(), this.getlinks(), null, "directParent", null, 0, 1,
				relationships.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getrelationships_UltimateParent(), this.getlinks(), null, "ultimateParent", null, 0, 1,
				relationships.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getrelationships_DirectChildren(), this.getlinks(), null, "directChildren", null, 0, 1,
				relationships.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(linksEClass, links.class, "links", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getlinks_LeiRecord(), this.getleiRecord(), null, "leiRecord", null, 0, 1, links.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getlinks_RelationshipRecord(), this.getleiRecord(), null, "relationshipRecord", null, 0, 1,
				links.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gleifEClass, gleif.class, "gleif", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getgleif_Records(), this.getleiRecord(), null, "records", null, 0, -1, gleif.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(entityStatusEEnum, entityStatus.class, "entityStatus");
		addEEnumLiteral(entityStatusEEnum, entityStatus.ACTIVE);
		addEEnumLiteral(entityStatusEEnum, entityStatus.INACTIVE);

		initEEnum(registartionStatusEEnum, registartionStatus.class, "registartionStatus");
		addEEnumLiteral(registartionStatusEEnum, registartionStatus.ISSUED);

		// Create resource
		createResource(eNS_URI);
	}

} //GleifmodelPackageImpl
