/**
 */
package gleifmodel.gleifmodel.impl;

import gleifmodel.gleifmodel.GleifmodelPackage;
import gleifmodel.gleifmodel.leiRecord;
import gleifmodel.gleifmodel.links;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>links</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.gleifmodel.impl.linksImpl#getLeiRecord <em>Lei Record</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.impl.linksImpl#getRelationshipRecord <em>Relationship Record</em>}</li>
 * </ul>
 *
 * @generated
 */
public class linksImpl extends MinimalEObjectImpl.Container implements links {
	/**
	 * The cached value of the '{@link #getLeiRecord() <em>Lei Record</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeiRecord()
	 * @generated
	 * @ordered
	 */
	protected leiRecord leiRecord;

	/**
	 * The cached value of the '{@link #getRelationshipRecord() <em>Relationship Record</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationshipRecord()
	 * @generated
	 * @ordered
	 */
	protected leiRecord relationshipRecord;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected linksImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.LINKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public leiRecord getLeiRecord() {
		if (leiRecord != null && leiRecord.eIsProxy()) {
			InternalEObject oldLeiRecord = (InternalEObject) leiRecord;
			leiRecord = (leiRecord) eResolveProxy(oldLeiRecord);
			if (leiRecord != oldLeiRecord) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GleifmodelPackage.LINKS__LEI_RECORD,
							oldLeiRecord, leiRecord));
			}
		}
		return leiRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public leiRecord basicGetLeiRecord() {
		return leiRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeiRecord(leiRecord newLeiRecord) {
		leiRecord oldLeiRecord = leiRecord;
		leiRecord = newLeiRecord;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.LINKS__LEI_RECORD, oldLeiRecord,
					leiRecord));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public leiRecord getRelationshipRecord() {
		if (relationshipRecord != null && relationshipRecord.eIsProxy()) {
			InternalEObject oldRelationshipRecord = (InternalEObject) relationshipRecord;
			relationshipRecord = (leiRecord) eResolveProxy(oldRelationshipRecord);
			if (relationshipRecord != oldRelationshipRecord) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.LINKS__RELATIONSHIP_RECORD, oldRelationshipRecord, relationshipRecord));
			}
		}
		return relationshipRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public leiRecord basicGetRelationshipRecord() {
		return relationshipRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelationshipRecord(leiRecord newRelationshipRecord) {
		leiRecord oldRelationshipRecord = relationshipRecord;
		relationshipRecord = newRelationshipRecord;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.LINKS__RELATIONSHIP_RECORD,
					oldRelationshipRecord, relationshipRecord));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.LINKS__LEI_RECORD:
			if (resolve)
				return getLeiRecord();
			return basicGetLeiRecord();
		case GleifmodelPackage.LINKS__RELATIONSHIP_RECORD:
			if (resolve)
				return getRelationshipRecord();
			return basicGetRelationshipRecord();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.LINKS__LEI_RECORD:
			setLeiRecord((leiRecord) newValue);
			return;
		case GleifmodelPackage.LINKS__RELATIONSHIP_RECORD:
			setRelationshipRecord((leiRecord) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.LINKS__LEI_RECORD:
			setLeiRecord((leiRecord) null);
			return;
		case GleifmodelPackage.LINKS__RELATIONSHIP_RECORD:
			setRelationshipRecord((leiRecord) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.LINKS__LEI_RECORD:
			return leiRecord != null;
		case GleifmodelPackage.LINKS__RELATIONSHIP_RECORD:
			return relationshipRecord != null;
		}
		return super.eIsSet(featureID);
	}

} //linksImpl
