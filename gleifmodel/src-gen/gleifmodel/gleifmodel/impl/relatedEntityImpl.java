/**
 */
package gleifmodel.gleifmodel.impl;

import gleifmodel.gleifmodel.GleifmodelPackage;
import gleifmodel.gleifmodel.relatedEntity;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>related Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.gleifmodel.impl.relatedEntityImpl#getLei <em>Lei</em>}</li>
 *   <li>{@link gleifmodel.gleifmodel.impl.relatedEntityImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class relatedEntityImpl extends MinimalEObjectImpl.Container implements relatedEntity {
	/**
	 * The default value of the '{@link #getLei() <em>Lei</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLei()
	 * @generated
	 * @ordered
	 */
	protected static final String LEI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLei() <em>Lei</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLei()
	 * @generated
	 * @ordered
	 */
	protected String lei = LEI_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected relatedEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.RELATED_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLei() {
		return lei;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLei(String newLei) {
		String oldLei = lei;
		lei = newLei;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATED_ENTITY__LEI, oldLei, lei));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATED_ENTITY__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.RELATED_ENTITY__LEI:
			return getLei();
		case GleifmodelPackage.RELATED_ENTITY__NAME:
			return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.RELATED_ENTITY__LEI:
			setLei((String) newValue);
			return;
		case GleifmodelPackage.RELATED_ENTITY__NAME:
			setName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.RELATED_ENTITY__LEI:
			setLei(LEI_EDEFAULT);
			return;
		case GleifmodelPackage.RELATED_ENTITY__NAME:
			setName(NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.RELATED_ENTITY__LEI:
			return LEI_EDEFAULT == null ? lei != null : !LEI_EDEFAULT.equals(lei);
		case GleifmodelPackage.RELATED_ENTITY__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lei: ");
		result.append(lei);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //relatedEntityImpl
