/**
 */
package gleifmodel.gleifmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>gleif</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.gleifmodel.gleif#getRecords <em>Records</em>}</li>
 * </ul>
 *
 * @see gleifmodel.gleifmodel.GleifmodelPackage#getgleif()
 * @model
 * @generated
 */
public interface gleif extends EObject {
	/**
	 * Returns the value of the '<em><b>Records</b></em>' reference list.
	 * The list contents are of type {@link gleifmodel.gleifmodel.leiRecord}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Records</em>' reference list.
	 * @see gleifmodel.gleifmodel.GleifmodelPackage#getgleif_Records()
	 * @model
	 * @generated
	 */
	EList<leiRecord> getRecords();

} // gleif
