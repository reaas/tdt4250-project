/**
 */
package gleifmodel.impl;

import gleifmodel.GleifmodelPackage;
import gleifmodel.links;
import gleifmodel.relationships;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>relationships</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.impl.relationshipsImpl#getManagingLou <em>Managing Lou</em>}</li>
 *   <li>{@link gleifmodel.impl.relationshipsImpl#getLeiIssuer <em>Lei Issuer</em>}</li>
 *   <li>{@link gleifmodel.impl.relationshipsImpl#getDirectParent <em>Direct Parent</em>}</li>
 *   <li>{@link gleifmodel.impl.relationshipsImpl#getUltimateParent <em>Ultimate Parent</em>}</li>
 *   <li>{@link gleifmodel.impl.relationshipsImpl#getDirectChildren <em>Direct Children</em>}</li>
 * </ul>
 *
 * @generated
 */
public class relationshipsImpl extends MinimalEObjectImpl.Container implements relationships {
	/**
	 * The cached value of the '{@link #getManagingLou() <em>Managing Lou</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManagingLou()
	 * @generated
	 * @ordered
	 */
	protected links managingLou;

	/**
	 * The cached value of the '{@link #getLeiIssuer() <em>Lei Issuer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeiIssuer()
	 * @generated
	 * @ordered
	 */
	protected links leiIssuer;

	/**
	 * The cached value of the '{@link #getDirectParent() <em>Direct Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectParent()
	 * @generated
	 * @ordered
	 */
	protected links directParent;

	/**
	 * The cached value of the '{@link #getUltimateParent() <em>Ultimate Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUltimateParent()
	 * @generated
	 * @ordered
	 */
	protected links ultimateParent;

	/**
	 * The cached value of the '{@link #getDirectChildren() <em>Direct Children</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectChildren()
	 * @generated
	 * @ordered
	 */
	protected links directChildren;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected relationshipsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.RELATIONSHIPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links getManagingLou() {
		if (managingLou != null && managingLou.eIsProxy()) {
			InternalEObject oldManagingLou = (InternalEObject) managingLou;
			managingLou = (links) eResolveProxy(oldManagingLou);
			if (managingLou != oldManagingLou) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.RELATIONSHIPS__MANAGING_LOU, oldManagingLou, managingLou));
			}
		}
		return managingLou;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links basicGetManagingLou() {
		return managingLou;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManagingLou(links newManagingLou) {
		links oldManagingLou = managingLou;
		managingLou = newManagingLou;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATIONSHIPS__MANAGING_LOU,
					oldManagingLou, managingLou));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links getLeiIssuer() {
		if (leiIssuer != null && leiIssuer.eIsProxy()) {
			InternalEObject oldLeiIssuer = (InternalEObject) leiIssuer;
			leiIssuer = (links) eResolveProxy(oldLeiIssuer);
			if (leiIssuer != oldLeiIssuer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.RELATIONSHIPS__LEI_ISSUER, oldLeiIssuer, leiIssuer));
			}
		}
		return leiIssuer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links basicGetLeiIssuer() {
		return leiIssuer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeiIssuer(links newLeiIssuer) {
		links oldLeiIssuer = leiIssuer;
		leiIssuer = newLeiIssuer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATIONSHIPS__LEI_ISSUER,
					oldLeiIssuer, leiIssuer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links getDirectParent() {
		if (directParent != null && directParent.eIsProxy()) {
			InternalEObject oldDirectParent = (InternalEObject) directParent;
			directParent = (links) eResolveProxy(oldDirectParent);
			if (directParent != oldDirectParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.RELATIONSHIPS__DIRECT_PARENT, oldDirectParent, directParent));
			}
		}
		return directParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links basicGetDirectParent() {
		return directParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectParent(links newDirectParent) {
		links oldDirectParent = directParent;
		directParent = newDirectParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATIONSHIPS__DIRECT_PARENT,
					oldDirectParent, directParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links getUltimateParent() {
		if (ultimateParent != null && ultimateParent.eIsProxy()) {
			InternalEObject oldUltimateParent = (InternalEObject) ultimateParent;
			ultimateParent = (links) eResolveProxy(oldUltimateParent);
			if (ultimateParent != oldUltimateParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.RELATIONSHIPS__ULTIMATE_PARENT, oldUltimateParent, ultimateParent));
			}
		}
		return ultimateParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links basicGetUltimateParent() {
		return ultimateParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUltimateParent(links newUltimateParent) {
		links oldUltimateParent = ultimateParent;
		ultimateParent = newUltimateParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATIONSHIPS__ULTIMATE_PARENT,
					oldUltimateParent, ultimateParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links getDirectChildren() {
		if (directChildren != null && directChildren.eIsProxy()) {
			InternalEObject oldDirectChildren = (InternalEObject) directChildren;
			directChildren = (links) eResolveProxy(oldDirectChildren);
			if (directChildren != oldDirectChildren) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.RELATIONSHIPS__DIRECT_CHILDREN, oldDirectChildren, directChildren));
			}
		}
		return directChildren;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links basicGetDirectChildren() {
		return directChildren;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectChildren(links newDirectChildren) {
		links oldDirectChildren = directChildren;
		directChildren = newDirectChildren;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.RELATIONSHIPS__DIRECT_CHILDREN,
					oldDirectChildren, directChildren));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.RELATIONSHIPS__MANAGING_LOU:
			if (resolve)
				return getManagingLou();
			return basicGetManagingLou();
		case GleifmodelPackage.RELATIONSHIPS__LEI_ISSUER:
			if (resolve)
				return getLeiIssuer();
			return basicGetLeiIssuer();
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_PARENT:
			if (resolve)
				return getDirectParent();
			return basicGetDirectParent();
		case GleifmodelPackage.RELATIONSHIPS__ULTIMATE_PARENT:
			if (resolve)
				return getUltimateParent();
			return basicGetUltimateParent();
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_CHILDREN:
			if (resolve)
				return getDirectChildren();
			return basicGetDirectChildren();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.RELATIONSHIPS__MANAGING_LOU:
			setManagingLou((links) newValue);
			return;
		case GleifmodelPackage.RELATIONSHIPS__LEI_ISSUER:
			setLeiIssuer((links) newValue);
			return;
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_PARENT:
			setDirectParent((links) newValue);
			return;
		case GleifmodelPackage.RELATIONSHIPS__ULTIMATE_PARENT:
			setUltimateParent((links) newValue);
			return;
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_CHILDREN:
			setDirectChildren((links) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.RELATIONSHIPS__MANAGING_LOU:
			setManagingLou((links) null);
			return;
		case GleifmodelPackage.RELATIONSHIPS__LEI_ISSUER:
			setLeiIssuer((links) null);
			return;
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_PARENT:
			setDirectParent((links) null);
			return;
		case GleifmodelPackage.RELATIONSHIPS__ULTIMATE_PARENT:
			setUltimateParent((links) null);
			return;
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_CHILDREN:
			setDirectChildren((links) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.RELATIONSHIPS__MANAGING_LOU:
			return managingLou != null;
		case GleifmodelPackage.RELATIONSHIPS__LEI_ISSUER:
			return leiIssuer != null;
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_PARENT:
			return directParent != null;
		case GleifmodelPackage.RELATIONSHIPS__ULTIMATE_PARENT:
			return ultimateParent != null;
		case GleifmodelPackage.RELATIONSHIPS__DIRECT_CHILDREN:
			return directChildren != null;
		}
		return super.eIsSet(featureID);
	}

} //relationshipsImpl
