/**
 */
package gleifmodel.impl;

import gleifmodel.GleifmodelPackage;
import gleifmodel.address;
import gleifmodel.company;
import gleifmodel.entityStatus;
import gleifmodel.legalName;
import gleifmodel.relatedEntity;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>company</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.impl.companyImpl#getLegalName <em>Legal Name</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getLegalAddress <em>Legal Address</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getHeadquartersAddress <em>Headquarters Address</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getRegisteredAs <em>Registered As</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getJurisdiction <em>Jurisdiction</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getAssociatedEntity <em>Associated Entity</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link gleifmodel.impl.companyImpl#getSuccessorEntity <em>Successor Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class companyImpl extends MinimalEObjectImpl.Container implements company {
	/**
	 * The cached value of the '{@link #getLegalName() <em>Legal Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLegalName()
	 * @generated
	 * @ordered
	 */
	protected legalName legalName;

	/**
	 * The cached value of the '{@link #getLegalAddress() <em>Legal Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLegalAddress()
	 * @generated
	 * @ordered
	 */
	protected address legalAddress;

	/**
	 * The cached value of the '{@link #getHeadquartersAddress() <em>Headquarters Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeadquartersAddress()
	 * @generated
	 * @ordered
	 */
	protected address headquartersAddress;

	/**
	 * The default value of the '{@link #getRegisteredAs() <em>Registered As</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredAs()
	 * @generated
	 * @ordered
	 */
	protected static final String REGISTERED_AS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRegisteredAs() <em>Registered As</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredAs()
	 * @generated
	 * @ordered
	 */
	protected String registeredAs = REGISTERED_AS_EDEFAULT;

	/**
	 * The default value of the '{@link #getJurisdiction() <em>Jurisdiction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJurisdiction()
	 * @generated
	 * @ordered
	 */
	protected static final String JURISDICTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJurisdiction() <em>Jurisdiction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJurisdiction()
	 * @generated
	 * @ordered
	 */
	protected String jurisdiction = JURISDICTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAssociatedEntity() <em>Associated Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedEntity()
	 * @generated
	 * @ordered
	 */
	protected relatedEntity associatedEntity;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final entityStatus STATUS_EDEFAULT = entityStatus.ACTIVE;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected entityStatus status = STATUS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuccessorEntity() <em>Successor Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuccessorEntity()
	 * @generated
	 * @ordered
	 */
	protected relatedEntity successorEntity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected companyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.COMPANY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public legalName getLegalName() {
		if (legalName != null && legalName.eIsProxy()) {
			InternalEObject oldLegalName = (InternalEObject) legalName;
			legalName = (legalName) eResolveProxy(oldLegalName);
			if (legalName != oldLegalName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GleifmodelPackage.COMPANY__LEGAL_NAME,
							oldLegalName, legalName));
			}
		}
		return legalName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public legalName basicGetLegalName() {
		return legalName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLegalName(legalName newLegalName) {
		legalName oldLegalName = legalName;
		legalName = newLegalName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__LEGAL_NAME, oldLegalName,
					legalName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public address getLegalAddress() {
		if (legalAddress != null && legalAddress.eIsProxy()) {
			InternalEObject oldLegalAddress = (InternalEObject) legalAddress;
			legalAddress = (address) eResolveProxy(oldLegalAddress);
			if (legalAddress != oldLegalAddress) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GleifmodelPackage.COMPANY__LEGAL_ADDRESS,
							oldLegalAddress, legalAddress));
			}
		}
		return legalAddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public address basicGetLegalAddress() {
		return legalAddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLegalAddress(address newLegalAddress) {
		address oldLegalAddress = legalAddress;
		legalAddress = newLegalAddress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__LEGAL_ADDRESS,
					oldLegalAddress, legalAddress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public address getHeadquartersAddress() {
		if (headquartersAddress != null && headquartersAddress.eIsProxy()) {
			InternalEObject oldHeadquartersAddress = (InternalEObject) headquartersAddress;
			headquartersAddress = (address) eResolveProxy(oldHeadquartersAddress);
			if (headquartersAddress != oldHeadquartersAddress) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.COMPANY__HEADQUARTERS_ADDRESS, oldHeadquartersAddress,
							headquartersAddress));
			}
		}
		return headquartersAddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public address basicGetHeadquartersAddress() {
		return headquartersAddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeadquartersAddress(address newHeadquartersAddress) {
		address oldHeadquartersAddress = headquartersAddress;
		headquartersAddress = newHeadquartersAddress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__HEADQUARTERS_ADDRESS,
					oldHeadquartersAddress, headquartersAddress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRegisteredAs() {
		return registeredAs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRegisteredAs(String newRegisteredAs) {
		String oldRegisteredAs = registeredAs;
		registeredAs = newRegisteredAs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__REGISTERED_AS,
					oldRegisteredAs, registeredAs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getJurisdiction() {
		return jurisdiction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJurisdiction(String newJurisdiction) {
		String oldJurisdiction = jurisdiction;
		jurisdiction = newJurisdiction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__JURISDICTION,
					oldJurisdiction, jurisdiction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public relatedEntity getAssociatedEntity() {
		if (associatedEntity != null && associatedEntity.eIsProxy()) {
			InternalEObject oldAssociatedEntity = (InternalEObject) associatedEntity;
			associatedEntity = (relatedEntity) eResolveProxy(oldAssociatedEntity);
			if (associatedEntity != oldAssociatedEntity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.COMPANY__ASSOCIATED_ENTITY, oldAssociatedEntity, associatedEntity));
			}
		}
		return associatedEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public relatedEntity basicGetAssociatedEntity() {
		return associatedEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedEntity(relatedEntity newAssociatedEntity) {
		relatedEntity oldAssociatedEntity = associatedEntity;
		associatedEntity = newAssociatedEntity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__ASSOCIATED_ENTITY,
					oldAssociatedEntity, associatedEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public entityStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(entityStatus newStatus) {
		entityStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__STATUS, oldStatus,
					status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public relatedEntity getSuccessorEntity() {
		if (successorEntity != null && successorEntity.eIsProxy()) {
			InternalEObject oldSuccessorEntity = (InternalEObject) successorEntity;
			successorEntity = (relatedEntity) eResolveProxy(oldSuccessorEntity);
			if (successorEntity != oldSuccessorEntity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.COMPANY__SUCCESSOR_ENTITY, oldSuccessorEntity, successorEntity));
			}
		}
		return successorEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public relatedEntity basicGetSuccessorEntity() {
		return successorEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuccessorEntity(relatedEntity newSuccessorEntity) {
		relatedEntity oldSuccessorEntity = successorEntity;
		successorEntity = newSuccessorEntity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.COMPANY__SUCCESSOR_ENTITY,
					oldSuccessorEntity, successorEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.COMPANY__LEGAL_NAME:
			if (resolve)
				return getLegalName();
			return basicGetLegalName();
		case GleifmodelPackage.COMPANY__LEGAL_ADDRESS:
			if (resolve)
				return getLegalAddress();
			return basicGetLegalAddress();
		case GleifmodelPackage.COMPANY__HEADQUARTERS_ADDRESS:
			if (resolve)
				return getHeadquartersAddress();
			return basicGetHeadquartersAddress();
		case GleifmodelPackage.COMPANY__REGISTERED_AS:
			return getRegisteredAs();
		case GleifmodelPackage.COMPANY__JURISDICTION:
			return getJurisdiction();
		case GleifmodelPackage.COMPANY__ASSOCIATED_ENTITY:
			if (resolve)
				return getAssociatedEntity();
			return basicGetAssociatedEntity();
		case GleifmodelPackage.COMPANY__STATUS:
			return getStatus();
		case GleifmodelPackage.COMPANY__SUCCESSOR_ENTITY:
			if (resolve)
				return getSuccessorEntity();
			return basicGetSuccessorEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.COMPANY__LEGAL_NAME:
			setLegalName((legalName) newValue);
			return;
		case GleifmodelPackage.COMPANY__LEGAL_ADDRESS:
			setLegalAddress((address) newValue);
			return;
		case GleifmodelPackage.COMPANY__HEADQUARTERS_ADDRESS:
			setHeadquartersAddress((address) newValue);
			return;
		case GleifmodelPackage.COMPANY__REGISTERED_AS:
			setRegisteredAs((String) newValue);
			return;
		case GleifmodelPackage.COMPANY__JURISDICTION:
			setJurisdiction((String) newValue);
			return;
		case GleifmodelPackage.COMPANY__ASSOCIATED_ENTITY:
			setAssociatedEntity((relatedEntity) newValue);
			return;
		case GleifmodelPackage.COMPANY__STATUS:
			setStatus((entityStatus) newValue);
			return;
		case GleifmodelPackage.COMPANY__SUCCESSOR_ENTITY:
			setSuccessorEntity((relatedEntity) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.COMPANY__LEGAL_NAME:
			setLegalName((legalName) null);
			return;
		case GleifmodelPackage.COMPANY__LEGAL_ADDRESS:
			setLegalAddress((address) null);
			return;
		case GleifmodelPackage.COMPANY__HEADQUARTERS_ADDRESS:
			setHeadquartersAddress((address) null);
			return;
		case GleifmodelPackage.COMPANY__REGISTERED_AS:
			setRegisteredAs(REGISTERED_AS_EDEFAULT);
			return;
		case GleifmodelPackage.COMPANY__JURISDICTION:
			setJurisdiction(JURISDICTION_EDEFAULT);
			return;
		case GleifmodelPackage.COMPANY__ASSOCIATED_ENTITY:
			setAssociatedEntity((relatedEntity) null);
			return;
		case GleifmodelPackage.COMPANY__STATUS:
			setStatus(STATUS_EDEFAULT);
			return;
		case GleifmodelPackage.COMPANY__SUCCESSOR_ENTITY:
			setSuccessorEntity((relatedEntity) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.COMPANY__LEGAL_NAME:
			return legalName != null;
		case GleifmodelPackage.COMPANY__LEGAL_ADDRESS:
			return legalAddress != null;
		case GleifmodelPackage.COMPANY__HEADQUARTERS_ADDRESS:
			return headquartersAddress != null;
		case GleifmodelPackage.COMPANY__REGISTERED_AS:
			return REGISTERED_AS_EDEFAULT == null ? registeredAs != null : !REGISTERED_AS_EDEFAULT.equals(registeredAs);
		case GleifmodelPackage.COMPANY__JURISDICTION:
			return JURISDICTION_EDEFAULT == null ? jurisdiction != null : !JURISDICTION_EDEFAULT.equals(jurisdiction);
		case GleifmodelPackage.COMPANY__ASSOCIATED_ENTITY:
			return associatedEntity != null;
		case GleifmodelPackage.COMPANY__STATUS:
			return status != STATUS_EDEFAULT;
		case GleifmodelPackage.COMPANY__SUCCESSOR_ENTITY:
			return successorEntity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (registeredAs: ");
		result.append(registeredAs);
		result.append(", jurisdiction: ");
		result.append(jurisdiction);
		result.append(", status: ");
		result.append(status);
		result.append(')');
		return result.toString();
	}

} //companyImpl
