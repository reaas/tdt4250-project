/**
 */
package gleifmodel.impl;

import gleifmodel.GleifmodelPackage;
import gleifmodel.company;
import gleifmodel.leiRecord;
import gleifmodel.links;
import gleifmodel.registration;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>lei Record</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.impl.leiRecordImpl#getLei <em>Lei</em>}</li>
 *   <li>{@link gleifmodel.impl.leiRecordImpl#getEntity <em>Entity</em>}</li>
 *   <li>{@link gleifmodel.impl.leiRecordImpl#getRegistration <em>Registration</em>}</li>
 *   <li>{@link gleifmodel.impl.leiRecordImpl#getLinks <em>Links</em>}</li>
 * </ul>
 *
 * @generated
 */
public class leiRecordImpl extends MinimalEObjectImpl.Container implements leiRecord {
	/**
	 * The default value of the '{@link #getLei() <em>Lei</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLei()
	 * @generated
	 * @ordered
	 */
	protected static final String LEI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLei() <em>Lei</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLei()
	 * @generated
	 * @ordered
	 */
	protected String lei = LEI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEntity() <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntity()
	 * @generated
	 * @ordered
	 */
	protected company entity;

	/**
	 * The cached value of the '{@link #getRegistration() <em>Registration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegistration()
	 * @generated
	 * @ordered
	 */
	protected registration registration;

	/**
	 * The cached value of the '{@link #getLinks() <em>Links</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinks()
	 * @generated
	 * @ordered
	 */
	protected links links;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected leiRecordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.LEI_RECORD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLei() {
		return lei;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLei(String newLei) {
		String oldLei = lei;
		lei = newLei;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.LEI_RECORD__LEI, oldLei, lei));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public company getEntity() {
		if (entity != null && entity.eIsProxy()) {
			InternalEObject oldEntity = (InternalEObject) entity;
			entity = (company) eResolveProxy(oldEntity);
			if (entity != oldEntity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GleifmodelPackage.LEI_RECORD__ENTITY,
							oldEntity, entity));
			}
		}
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public company basicGetEntity() {
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntity(company newEntity) {
		company oldEntity = entity;
		entity = newEntity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.LEI_RECORD__ENTITY, oldEntity,
					entity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public registration getRegistration() {
		if (registration != null && registration.eIsProxy()) {
			InternalEObject oldRegistration = (InternalEObject) registration;
			registration = (registration) eResolveProxy(oldRegistration);
			if (registration != oldRegistration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							GleifmodelPackage.LEI_RECORD__REGISTRATION, oldRegistration, registration));
			}
		}
		return registration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public registration basicGetRegistration() {
		return registration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRegistration(registration newRegistration) {
		registration oldRegistration = registration;
		registration = newRegistration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.LEI_RECORD__REGISTRATION,
					oldRegistration, registration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links getLinks() {
		if (links != null && links.eIsProxy()) {
			InternalEObject oldLinks = (InternalEObject) links;
			links = (links) eResolveProxy(oldLinks);
			if (links != oldLinks) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GleifmodelPackage.LEI_RECORD__LINKS,
							oldLinks, links));
			}
		}
		return links;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public links basicGetLinks() {
		return links;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinks(links newLinks) {
		links oldLinks = links;
		links = newLinks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.LEI_RECORD__LINKS, oldLinks,
					links));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.LEI_RECORD__LEI:
			return getLei();
		case GleifmodelPackage.LEI_RECORD__ENTITY:
			if (resolve)
				return getEntity();
			return basicGetEntity();
		case GleifmodelPackage.LEI_RECORD__REGISTRATION:
			if (resolve)
				return getRegistration();
			return basicGetRegistration();
		case GleifmodelPackage.LEI_RECORD__LINKS:
			if (resolve)
				return getLinks();
			return basicGetLinks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.LEI_RECORD__LEI:
			setLei((String) newValue);
			return;
		case GleifmodelPackage.LEI_RECORD__ENTITY:
			setEntity((company) newValue);
			return;
		case GleifmodelPackage.LEI_RECORD__REGISTRATION:
			setRegistration((registration) newValue);
			return;
		case GleifmodelPackage.LEI_RECORD__LINKS:
			setLinks((links) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.LEI_RECORD__LEI:
			setLei(LEI_EDEFAULT);
			return;
		case GleifmodelPackage.LEI_RECORD__ENTITY:
			setEntity((company) null);
			return;
		case GleifmodelPackage.LEI_RECORD__REGISTRATION:
			setRegistration((registration) null);
			return;
		case GleifmodelPackage.LEI_RECORD__LINKS:
			setLinks((links) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.LEI_RECORD__LEI:
			return LEI_EDEFAULT == null ? lei != null : !LEI_EDEFAULT.equals(lei);
		case GleifmodelPackage.LEI_RECORD__ENTITY:
			return entity != null;
		case GleifmodelPackage.LEI_RECORD__REGISTRATION:
			return registration != null;
		case GleifmodelPackage.LEI_RECORD__LINKS:
			return links != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lei: ");
		result.append(lei);
		result.append(')');
		return result.toString();
	}

} //leiRecordImpl
