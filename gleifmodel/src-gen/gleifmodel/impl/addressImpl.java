/**
 */
package gleifmodel.impl;

import gleifmodel.GleifmodelPackage;
import gleifmodel.address;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>address</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.impl.addressImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getAddressLines <em>Address Lines</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getAddressNumber <em>Address Number</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getAddressNumberWithinBuilding <em>Address Number Within Building</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getMailRouting <em>Mail Routing</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getCity <em>City</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getRegion <em>Region</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getCountry <em>Country</em>}</li>
 *   <li>{@link gleifmodel.impl.addressImpl#getPostalCode <em>Postal Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class addressImpl extends MinimalEObjectImpl.Container implements address {
	/**
	 * The default value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected String language = LANGUAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAddressLines() <em>Address Lines</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddressLines()
	 * @generated
	 * @ordered
	 */
	protected EList<?> addressLines;

	/**
	 * The default value of the '{@link #getAddressNumber() <em>Address Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddressNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String ADDRESS_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAddressNumber() <em>Address Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddressNumber()
	 * @generated
	 * @ordered
	 */
	protected String addressNumber = ADDRESS_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getAddressNumberWithinBuilding() <em>Address Number Within Building</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddressNumberWithinBuilding()
	 * @generated
	 * @ordered
	 */
	protected static final String ADDRESS_NUMBER_WITHIN_BUILDING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAddressNumberWithinBuilding() <em>Address Number Within Building</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddressNumberWithinBuilding()
	 * @generated
	 * @ordered
	 */
	protected String addressNumberWithinBuilding = ADDRESS_NUMBER_WITHIN_BUILDING_EDEFAULT;

	/**
	 * The default value of the '{@link #getMailRouting() <em>Mail Routing</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMailRouting()
	 * @generated
	 * @ordered
	 */
	protected static final String MAIL_ROUTING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMailRouting() <em>Mail Routing</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMailRouting()
	 * @generated
	 * @ordered
	 */
	protected String mailRouting = MAIL_ROUTING_EDEFAULT;

	/**
	 * The default value of the '{@link #getCity() <em>City</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCity()
	 * @generated
	 * @ordered
	 */
	protected static final String CITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCity() <em>City</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCity()
	 * @generated
	 * @ordered
	 */
	protected String city = CITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getRegion() <em>Region</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegion()
	 * @generated
	 * @ordered
	 */
	protected static final String REGION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRegion() <em>Region</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegion()
	 * @generated
	 * @ordered
	 */
	protected String region = REGION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCountry() <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountry()
	 * @generated
	 * @ordered
	 */
	protected static final String COUNTRY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCountry() <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountry()
	 * @generated
	 * @ordered
	 */
	protected String country = COUNTRY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPostalCode() <em>Postal Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostalCode()
	 * @generated
	 * @ordered
	 */
	protected static final String POSTAL_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPostalCode() <em>Postal Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostalCode()
	 * @generated
	 * @ordered
	 */
	protected String postalCode = POSTAL_CODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected addressImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.ADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguage(String newLanguage) {
		String oldLanguage = language;
		language = newLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__LANGUAGE, oldLanguage,
					language));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<?> getAddressLines() {
		return addressLines;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddressLines(EList<?> newAddressLines) {
		EList<?> oldAddressLines = addressLines;
		addressLines = newAddressLines;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__ADDRESS_LINES,
					oldAddressLines, addressLines));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAddressNumber() {
		return addressNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddressNumber(String newAddressNumber) {
		String oldAddressNumber = addressNumber;
		addressNumber = newAddressNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__ADDRESS_NUMBER,
					oldAddressNumber, addressNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAddressNumberWithinBuilding() {
		return addressNumberWithinBuilding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddressNumberWithinBuilding(String newAddressNumberWithinBuilding) {
		String oldAddressNumberWithinBuilding = addressNumberWithinBuilding;
		addressNumberWithinBuilding = newAddressNumberWithinBuilding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					GleifmodelPackage.ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING, oldAddressNumberWithinBuilding,
					addressNumberWithinBuilding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMailRouting() {
		return mailRouting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMailRouting(String newMailRouting) {
		String oldMailRouting = mailRouting;
		mailRouting = newMailRouting;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__MAIL_ROUTING,
					oldMailRouting, mailRouting));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCity() {
		return city;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCity(String newCity) {
		String oldCity = city;
		city = newCity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__CITY, oldCity, city));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRegion(String newRegion) {
		String oldRegion = region;
		region = newRegion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__REGION, oldRegion,
					region));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountry(String newCountry) {
		String oldCountry = country;
		country = newCountry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__COUNTRY, oldCountry,
					country));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostalCode(String newPostalCode) {
		String oldPostalCode = postalCode;
		postalCode = newPostalCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.ADDRESS__POSTAL_CODE, oldPostalCode,
					postalCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.ADDRESS__LANGUAGE:
			return getLanguage();
		case GleifmodelPackage.ADDRESS__ADDRESS_LINES:
			return getAddressLines();
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER:
			return getAddressNumber();
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING:
			return getAddressNumberWithinBuilding();
		case GleifmodelPackage.ADDRESS__MAIL_ROUTING:
			return getMailRouting();
		case GleifmodelPackage.ADDRESS__CITY:
			return getCity();
		case GleifmodelPackage.ADDRESS__REGION:
			return getRegion();
		case GleifmodelPackage.ADDRESS__COUNTRY:
			return getCountry();
		case GleifmodelPackage.ADDRESS__POSTAL_CODE:
			return getPostalCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.ADDRESS__LANGUAGE:
			setLanguage((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__ADDRESS_LINES:
			setAddressLines((EList<?>) newValue);
			return;
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER:
			setAddressNumber((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING:
			setAddressNumberWithinBuilding((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__MAIL_ROUTING:
			setMailRouting((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__CITY:
			setCity((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__REGION:
			setRegion((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__COUNTRY:
			setCountry((String) newValue);
			return;
		case GleifmodelPackage.ADDRESS__POSTAL_CODE:
			setPostalCode((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.ADDRESS__LANGUAGE:
			setLanguage(LANGUAGE_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__ADDRESS_LINES:
			setAddressLines((EList<?>) null);
			return;
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER:
			setAddressNumber(ADDRESS_NUMBER_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING:
			setAddressNumberWithinBuilding(ADDRESS_NUMBER_WITHIN_BUILDING_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__MAIL_ROUTING:
			setMailRouting(MAIL_ROUTING_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__CITY:
			setCity(CITY_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__REGION:
			setRegion(REGION_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__COUNTRY:
			setCountry(COUNTRY_EDEFAULT);
			return;
		case GleifmodelPackage.ADDRESS__POSTAL_CODE:
			setPostalCode(POSTAL_CODE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.ADDRESS__LANGUAGE:
			return LANGUAGE_EDEFAULT == null ? language != null : !LANGUAGE_EDEFAULT.equals(language);
		case GleifmodelPackage.ADDRESS__ADDRESS_LINES:
			return addressLines != null;
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER:
			return ADDRESS_NUMBER_EDEFAULT == null ? addressNumber != null
					: !ADDRESS_NUMBER_EDEFAULT.equals(addressNumber);
		case GleifmodelPackage.ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING:
			return ADDRESS_NUMBER_WITHIN_BUILDING_EDEFAULT == null ? addressNumberWithinBuilding != null
					: !ADDRESS_NUMBER_WITHIN_BUILDING_EDEFAULT.equals(addressNumberWithinBuilding);
		case GleifmodelPackage.ADDRESS__MAIL_ROUTING:
			return MAIL_ROUTING_EDEFAULT == null ? mailRouting != null : !MAIL_ROUTING_EDEFAULT.equals(mailRouting);
		case GleifmodelPackage.ADDRESS__CITY:
			return CITY_EDEFAULT == null ? city != null : !CITY_EDEFAULT.equals(city);
		case GleifmodelPackage.ADDRESS__REGION:
			return REGION_EDEFAULT == null ? region != null : !REGION_EDEFAULT.equals(region);
		case GleifmodelPackage.ADDRESS__COUNTRY:
			return COUNTRY_EDEFAULT == null ? country != null : !COUNTRY_EDEFAULT.equals(country);
		case GleifmodelPackage.ADDRESS__POSTAL_CODE:
			return POSTAL_CODE_EDEFAULT == null ? postalCode != null : !POSTAL_CODE_EDEFAULT.equals(postalCode);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (language: ");
		result.append(language);
		result.append(", addressLines: ");
		result.append(addressLines);
		result.append(", addressNumber: ");
		result.append(addressNumber);
		result.append(", addressNumberWithinBuilding: ");
		result.append(addressNumberWithinBuilding);
		result.append(", mailRouting: ");
		result.append(mailRouting);
		result.append(", city: ");
		result.append(city);
		result.append(", region: ");
		result.append(region);
		result.append(", country: ");
		result.append(country);
		result.append(", postalCode: ");
		result.append(postalCode);
		result.append(')');
		return result.toString();
	}

} //addressImpl
