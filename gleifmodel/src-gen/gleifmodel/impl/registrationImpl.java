/**
 */
package gleifmodel.impl;

import gleifmodel.GleifmodelPackage;
import gleifmodel.registartionStatus;
import gleifmodel.registration;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>registration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.impl.registrationImpl#getInitialRegistrationDate <em>Initial Registration Date</em>}</li>
 *   <li>{@link gleifmodel.impl.registrationImpl#getLastUpdateDate <em>Last Update Date</em>}</li>
 *   <li>{@link gleifmodel.impl.registrationImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link gleifmodel.impl.registrationImpl#getNextRenewalDate <em>Next Renewal Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class registrationImpl extends MinimalEObjectImpl.Container implements registration {
	/**
	 * The default value of the '{@link #getInitialRegistrationDate() <em>Initial Registration Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialRegistrationDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date INITIAL_REGISTRATION_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitialRegistrationDate() <em>Initial Registration Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialRegistrationDate()
	 * @generated
	 * @ordered
	 */
	protected Date initialRegistrationDate = INITIAL_REGISTRATION_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastUpdateDate() <em>Last Update Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastUpdateDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date LAST_UPDATE_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastUpdateDate() <em>Last Update Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastUpdateDate()
	 * @generated
	 * @ordered
	 */
	protected Date lastUpdateDate = LAST_UPDATE_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final registartionStatus STATUS_EDEFAULT = registartionStatus.ISSUED;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected registartionStatus status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNextRenewalDate() <em>Next Renewal Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextRenewalDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date NEXT_RENEWAL_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNextRenewalDate() <em>Next Renewal Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextRenewalDate()
	 * @generated
	 * @ordered
	 */
	protected Date nextRenewalDate = NEXT_RENEWAL_DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected registrationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GleifmodelPackage.Literals.REGISTRATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getInitialRegistrationDate() {
		return initialRegistrationDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialRegistrationDate(Date newInitialRegistrationDate) {
		Date oldInitialRegistrationDate = initialRegistrationDate;
		initialRegistrationDate = newInitialRegistrationDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					GleifmodelPackage.REGISTRATION__INITIAL_REGISTRATION_DATE, oldInitialRegistrationDate,
					initialRegistrationDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastUpdateDate(Date newLastUpdateDate) {
		Date oldLastUpdateDate = lastUpdateDate;
		lastUpdateDate = newLastUpdateDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.REGISTRATION__LAST_UPDATE_DATE,
					oldLastUpdateDate, lastUpdateDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public registartionStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(registartionStatus newStatus) {
		registartionStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.REGISTRATION__STATUS, oldStatus,
					status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getNextRenewalDate() {
		return nextRenewalDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextRenewalDate(Date newNextRenewalDate) {
		Date oldNextRenewalDate = nextRenewalDate;
		nextRenewalDate = newNextRenewalDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GleifmodelPackage.REGISTRATION__NEXT_RENEWAL_DATE,
					oldNextRenewalDate, nextRenewalDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case GleifmodelPackage.REGISTRATION__INITIAL_REGISTRATION_DATE:
			return getInitialRegistrationDate();
		case GleifmodelPackage.REGISTRATION__LAST_UPDATE_DATE:
			return getLastUpdateDate();
		case GleifmodelPackage.REGISTRATION__STATUS:
			return getStatus();
		case GleifmodelPackage.REGISTRATION__NEXT_RENEWAL_DATE:
			return getNextRenewalDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case GleifmodelPackage.REGISTRATION__INITIAL_REGISTRATION_DATE:
			setInitialRegistrationDate((Date) newValue);
			return;
		case GleifmodelPackage.REGISTRATION__LAST_UPDATE_DATE:
			setLastUpdateDate((Date) newValue);
			return;
		case GleifmodelPackage.REGISTRATION__STATUS:
			setStatus((registartionStatus) newValue);
			return;
		case GleifmodelPackage.REGISTRATION__NEXT_RENEWAL_DATE:
			setNextRenewalDate((Date) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.REGISTRATION__INITIAL_REGISTRATION_DATE:
			setInitialRegistrationDate(INITIAL_REGISTRATION_DATE_EDEFAULT);
			return;
		case GleifmodelPackage.REGISTRATION__LAST_UPDATE_DATE:
			setLastUpdateDate(LAST_UPDATE_DATE_EDEFAULT);
			return;
		case GleifmodelPackage.REGISTRATION__STATUS:
			setStatus(STATUS_EDEFAULT);
			return;
		case GleifmodelPackage.REGISTRATION__NEXT_RENEWAL_DATE:
			setNextRenewalDate(NEXT_RENEWAL_DATE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case GleifmodelPackage.REGISTRATION__INITIAL_REGISTRATION_DATE:
			return INITIAL_REGISTRATION_DATE_EDEFAULT == null ? initialRegistrationDate != null
					: !INITIAL_REGISTRATION_DATE_EDEFAULT.equals(initialRegistrationDate);
		case GleifmodelPackage.REGISTRATION__LAST_UPDATE_DATE:
			return LAST_UPDATE_DATE_EDEFAULT == null ? lastUpdateDate != null
					: !LAST_UPDATE_DATE_EDEFAULT.equals(lastUpdateDate);
		case GleifmodelPackage.REGISTRATION__STATUS:
			return status != STATUS_EDEFAULT;
		case GleifmodelPackage.REGISTRATION__NEXT_RENEWAL_DATE:
			return NEXT_RENEWAL_DATE_EDEFAULT == null ? nextRenewalDate != null
					: !NEXT_RENEWAL_DATE_EDEFAULT.equals(nextRenewalDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (initialRegistrationDate: ");
		result.append(initialRegistrationDate);
		result.append(", lastUpdateDate: ");
		result.append(lastUpdateDate);
		result.append(", status: ");
		result.append(status);
		result.append(", nextRenewalDate: ");
		result.append(nextRenewalDate);
		result.append(')');
		return result.toString();
	}

} //registrationImpl
