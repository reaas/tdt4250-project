/**
 */
package gleifmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gleifmodel.GleifmodelFactory
 * @model kind="package"
 * @generated
 */
public interface GleifmodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gleifmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/gleifmodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "gleifmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GleifmodelPackage eINSTANCE = gleifmodel.impl.GleifmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link gleifmodel.impl.legalNameImpl <em>legal Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.legalNameImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getlegalName()
	 * @generated
	 */
	int LEGAL_NAME = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGAL_NAME__NAME = 0;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGAL_NAME__LANGUAGE = 1;

	/**
	 * The number of structural features of the '<em>legal Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGAL_NAME_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>legal Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGAL_NAME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.addressImpl <em>address</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.addressImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getaddress()
	 * @generated
	 */
	int ADDRESS = 1;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__LANGUAGE = 0;

	/**
	 * The feature id for the '<em><b>Address Lines</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__ADDRESS_LINES = 1;

	/**
	 * The feature id for the '<em><b>Address Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__ADDRESS_NUMBER = 2;

	/**
	 * The feature id for the '<em><b>Address Number Within Building</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING = 3;

	/**
	 * The feature id for the '<em><b>Mail Routing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__MAIL_ROUTING = 4;

	/**
	 * The feature id for the '<em><b>City</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__CITY = 5;

	/**
	 * The feature id for the '<em><b>Region</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__REGION = 6;

	/**
	 * The feature id for the '<em><b>Country</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__COUNTRY = 7;

	/**
	 * The feature id for the '<em><b>Postal Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__POSTAL_CODE = 8;

	/**
	 * The number of structural features of the '<em>address</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>address</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.relatedEntityImpl <em>related Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.relatedEntityImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getrelatedEntity()
	 * @generated
	 */
	int RELATED_ENTITY = 2;

	/**
	 * The feature id for the '<em><b>Lei</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_ENTITY__LEI = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_ENTITY__NAME = 1;

	/**
	 * The number of structural features of the '<em>related Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_ENTITY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>related Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.companyImpl <em>company</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.companyImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getcompany()
	 * @generated
	 */
	int COMPANY = 3;

	/**
	 * The feature id for the '<em><b>Legal Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__LEGAL_NAME = 0;

	/**
	 * The feature id for the '<em><b>Legal Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__LEGAL_ADDRESS = 1;

	/**
	 * The feature id for the '<em><b>Headquarters Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__HEADQUARTERS_ADDRESS = 2;

	/**
	 * The feature id for the '<em><b>Registered As</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__REGISTERED_AS = 3;

	/**
	 * The feature id for the '<em><b>Jurisdiction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__JURISDICTION = 4;

	/**
	 * The feature id for the '<em><b>Associated Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__ASSOCIATED_ENTITY = 5;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__STATUS = 6;

	/**
	 * The feature id for the '<em><b>Successor Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY__SUCCESSOR_ENTITY = 7;

	/**
	 * The number of structural features of the '<em>company</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>company</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPANY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.registrationImpl <em>registration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.registrationImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getregistration()
	 * @generated
	 */
	int REGISTRATION = 4;

	/**
	 * The feature id for the '<em><b>Initial Registration Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRATION__INITIAL_REGISTRATION_DATE = 0;

	/**
	 * The feature id for the '<em><b>Last Update Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRATION__LAST_UPDATE_DATE = 1;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRATION__STATUS = 2;

	/**
	 * The feature id for the '<em><b>Next Renewal Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRATION__NEXT_RENEWAL_DATE = 3;

	/**
	 * The number of structural features of the '<em>registration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>registration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.leiRecordImpl <em>lei Record</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.leiRecordImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getleiRecord()
	 * @generated
	 */
	int LEI_RECORD = 5;

	/**
	 * The feature id for the '<em><b>Lei</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEI_RECORD__LEI = 0;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEI_RECORD__ENTITY = 1;

	/**
	 * The feature id for the '<em><b>Registration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEI_RECORD__REGISTRATION = 2;

	/**
	 * The feature id for the '<em><b>Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEI_RECORD__LINKS = 3;

	/**
	 * The number of structural features of the '<em>lei Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEI_RECORD_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>lei Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEI_RECORD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.relationshipsImpl <em>relationships</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.relationshipsImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getrelationships()
	 * @generated
	 */
	int RELATIONSHIPS = 6;

	/**
	 * The feature id for the '<em><b>Managing Lou</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS__MANAGING_LOU = 0;

	/**
	 * The feature id for the '<em><b>Lei Issuer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS__LEI_ISSUER = 1;

	/**
	 * The feature id for the '<em><b>Direct Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS__DIRECT_PARENT = 2;

	/**
	 * The feature id for the '<em><b>Ultimate Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS__ULTIMATE_PARENT = 3;

	/**
	 * The feature id for the '<em><b>Direct Children</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS__DIRECT_CHILDREN = 4;

	/**
	 * The number of structural features of the '<em>relationships</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>relationships</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIPS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.impl.linksImpl <em>links</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.impl.linksImpl
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getlinks()
	 * @generated
	 */
	int LINKS = 7;

	/**
	 * The feature id for the '<em><b>Lei Record</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINKS__LEI_RECORD = 0;

	/**
	 * The feature id for the '<em><b>Relationship Record</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINKS__RELATIONSHIP_RECORD = 1;

	/**
	 * The number of structural features of the '<em>links</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINKS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>links</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINKS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link gleifmodel.entityStatus <em>entity Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.entityStatus
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getentityStatus()
	 * @generated
	 */
	int ENTITY_STATUS = 8;

	/**
	 * The meta object id for the '{@link gleifmodel.registartionStatus <em>registartion Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gleifmodel.registartionStatus
	 * @see gleifmodel.impl.GleifmodelPackageImpl#getregistartionStatus()
	 * @generated
	 */
	int REGISTARTION_STATUS = 9;

	/**
	 * Returns the meta object for class '{@link gleifmodel.legalName <em>legal Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>legal Name</em>'.
	 * @see gleifmodel.legalName
	 * @generated
	 */
	EClass getlegalName();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.legalName#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gleifmodel.legalName#getName()
	 * @see #getlegalName()
	 * @generated
	 */
	EAttribute getlegalName_Name();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.legalName#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see gleifmodel.legalName#getLanguage()
	 * @see #getlegalName()
	 * @generated
	 */
	EAttribute getlegalName_Language();

	/**
	 * Returns the meta object for class '{@link gleifmodel.address <em>address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>address</em>'.
	 * @see gleifmodel.address
	 * @generated
	 */
	EClass getaddress();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see gleifmodel.address#getLanguage()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_Language();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getAddressLines <em>Address Lines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address Lines</em>'.
	 * @see gleifmodel.address#getAddressLines()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_AddressLines();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getAddressNumber <em>Address Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address Number</em>'.
	 * @see gleifmodel.address#getAddressNumber()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_AddressNumber();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getAddressNumberWithinBuilding <em>Address Number Within Building</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address Number Within Building</em>'.
	 * @see gleifmodel.address#getAddressNumberWithinBuilding()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_AddressNumberWithinBuilding();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getMailRouting <em>Mail Routing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mail Routing</em>'.
	 * @see gleifmodel.address#getMailRouting()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_MailRouting();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getCity <em>City</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>City</em>'.
	 * @see gleifmodel.address#getCity()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_City();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getRegion <em>Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Region</em>'.
	 * @see gleifmodel.address#getRegion()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_Region();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getCountry <em>Country</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Country</em>'.
	 * @see gleifmodel.address#getCountry()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_Country();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.address#getPostalCode <em>Postal Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postal Code</em>'.
	 * @see gleifmodel.address#getPostalCode()
	 * @see #getaddress()
	 * @generated
	 */
	EAttribute getaddress_PostalCode();

	/**
	 * Returns the meta object for class '{@link gleifmodel.relatedEntity <em>related Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>related Entity</em>'.
	 * @see gleifmodel.relatedEntity
	 * @generated
	 */
	EClass getrelatedEntity();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.relatedEntity#getLei <em>Lei</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lei</em>'.
	 * @see gleifmodel.relatedEntity#getLei()
	 * @see #getrelatedEntity()
	 * @generated
	 */
	EAttribute getrelatedEntity_Lei();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.relatedEntity#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gleifmodel.relatedEntity#getName()
	 * @see #getrelatedEntity()
	 * @generated
	 */
	EAttribute getrelatedEntity_Name();

	/**
	 * Returns the meta object for class '{@link gleifmodel.company <em>company</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>company</em>'.
	 * @see gleifmodel.company
	 * @generated
	 */
	EClass getcompany();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.company#getLegalName <em>Legal Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Legal Name</em>'.
	 * @see gleifmodel.company#getLegalName()
	 * @see #getcompany()
	 * @generated
	 */
	EReference getcompany_LegalName();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.company#getLegalAddress <em>Legal Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Legal Address</em>'.
	 * @see gleifmodel.company#getLegalAddress()
	 * @see #getcompany()
	 * @generated
	 */
	EReference getcompany_LegalAddress();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.company#getHeadquartersAddress <em>Headquarters Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Headquarters Address</em>'.
	 * @see gleifmodel.company#getHeadquartersAddress()
	 * @see #getcompany()
	 * @generated
	 */
	EReference getcompany_HeadquartersAddress();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.company#getRegisteredAs <em>Registered As</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Registered As</em>'.
	 * @see gleifmodel.company#getRegisteredAs()
	 * @see #getcompany()
	 * @generated
	 */
	EAttribute getcompany_RegisteredAs();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.company#getJurisdiction <em>Jurisdiction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jurisdiction</em>'.
	 * @see gleifmodel.company#getJurisdiction()
	 * @see #getcompany()
	 * @generated
	 */
	EAttribute getcompany_Jurisdiction();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.company#getAssociatedEntity <em>Associated Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Entity</em>'.
	 * @see gleifmodel.company#getAssociatedEntity()
	 * @see #getcompany()
	 * @generated
	 */
	EReference getcompany_AssociatedEntity();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.company#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see gleifmodel.company#getStatus()
	 * @see #getcompany()
	 * @generated
	 */
	EAttribute getcompany_Status();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.company#getSuccessorEntity <em>Successor Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Successor Entity</em>'.
	 * @see gleifmodel.company#getSuccessorEntity()
	 * @see #getcompany()
	 * @generated
	 */
	EReference getcompany_SuccessorEntity();

	/**
	 * Returns the meta object for class '{@link gleifmodel.registration <em>registration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>registration</em>'.
	 * @see gleifmodel.registration
	 * @generated
	 */
	EClass getregistration();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.registration#getInitialRegistrationDate <em>Initial Registration Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Registration Date</em>'.
	 * @see gleifmodel.registration#getInitialRegistrationDate()
	 * @see #getregistration()
	 * @generated
	 */
	EAttribute getregistration_InitialRegistrationDate();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.registration#getLastUpdateDate <em>Last Update Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Update Date</em>'.
	 * @see gleifmodel.registration#getLastUpdateDate()
	 * @see #getregistration()
	 * @generated
	 */
	EAttribute getregistration_LastUpdateDate();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.registration#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see gleifmodel.registration#getStatus()
	 * @see #getregistration()
	 * @generated
	 */
	EAttribute getregistration_Status();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.registration#getNextRenewalDate <em>Next Renewal Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Next Renewal Date</em>'.
	 * @see gleifmodel.registration#getNextRenewalDate()
	 * @see #getregistration()
	 * @generated
	 */
	EAttribute getregistration_NextRenewalDate();

	/**
	 * Returns the meta object for class '{@link gleifmodel.leiRecord <em>lei Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>lei Record</em>'.
	 * @see gleifmodel.leiRecord
	 * @generated
	 */
	EClass getleiRecord();

	/**
	 * Returns the meta object for the attribute '{@link gleifmodel.leiRecord#getLei <em>Lei</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lei</em>'.
	 * @see gleifmodel.leiRecord#getLei()
	 * @see #getleiRecord()
	 * @generated
	 */
	EAttribute getleiRecord_Lei();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.leiRecord#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity</em>'.
	 * @see gleifmodel.leiRecord#getEntity()
	 * @see #getleiRecord()
	 * @generated
	 */
	EReference getleiRecord_Entity();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.leiRecord#getRegistration <em>Registration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Registration</em>'.
	 * @see gleifmodel.leiRecord#getRegistration()
	 * @see #getleiRecord()
	 * @generated
	 */
	EReference getleiRecord_Registration();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.leiRecord#getLinks <em>Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Links</em>'.
	 * @see gleifmodel.leiRecord#getLinks()
	 * @see #getleiRecord()
	 * @generated
	 */
	EReference getleiRecord_Links();

	/**
	 * Returns the meta object for class '{@link gleifmodel.relationships <em>relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>relationships</em>'.
	 * @see gleifmodel.relationships
	 * @generated
	 */
	EClass getrelationships();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.relationships#getManagingLou <em>Managing Lou</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Managing Lou</em>'.
	 * @see gleifmodel.relationships#getManagingLou()
	 * @see #getrelationships()
	 * @generated
	 */
	EReference getrelationships_ManagingLou();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.relationships#getLeiIssuer <em>Lei Issuer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lei Issuer</em>'.
	 * @see gleifmodel.relationships#getLeiIssuer()
	 * @see #getrelationships()
	 * @generated
	 */
	EReference getrelationships_LeiIssuer();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.relationships#getDirectParent <em>Direct Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Direct Parent</em>'.
	 * @see gleifmodel.relationships#getDirectParent()
	 * @see #getrelationships()
	 * @generated
	 */
	EReference getrelationships_DirectParent();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.relationships#getUltimateParent <em>Ultimate Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ultimate Parent</em>'.
	 * @see gleifmodel.relationships#getUltimateParent()
	 * @see #getrelationships()
	 * @generated
	 */
	EReference getrelationships_UltimateParent();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.relationships#getDirectChildren <em>Direct Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Direct Children</em>'.
	 * @see gleifmodel.relationships#getDirectChildren()
	 * @see #getrelationships()
	 * @generated
	 */
	EReference getrelationships_DirectChildren();

	/**
	 * Returns the meta object for class '{@link gleifmodel.links <em>links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>links</em>'.
	 * @see gleifmodel.links
	 * @generated
	 */
	EClass getlinks();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.links#getLeiRecord <em>Lei Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lei Record</em>'.
	 * @see gleifmodel.links#getLeiRecord()
	 * @see #getlinks()
	 * @generated
	 */
	EReference getlinks_LeiRecord();

	/**
	 * Returns the meta object for the reference '{@link gleifmodel.links#getRelationshipRecord <em>Relationship Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Relationship Record</em>'.
	 * @see gleifmodel.links#getRelationshipRecord()
	 * @see #getlinks()
	 * @generated
	 */
	EReference getlinks_RelationshipRecord();

	/**
	 * Returns the meta object for enum '{@link gleifmodel.entityStatus <em>entity Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>entity Status</em>'.
	 * @see gleifmodel.entityStatus
	 * @generated
	 */
	EEnum getentityStatus();

	/**
	 * Returns the meta object for enum '{@link gleifmodel.registartionStatus <em>registartion Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>registartion Status</em>'.
	 * @see gleifmodel.registartionStatus
	 * @generated
	 */
	EEnum getregistartionStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GleifmodelFactory getGleifmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gleifmodel.impl.legalNameImpl <em>legal Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.legalNameImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getlegalName()
		 * @generated
		 */
		EClass LEGAL_NAME = eINSTANCE.getlegalName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGAL_NAME__NAME = eINSTANCE.getlegalName_Name();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGAL_NAME__LANGUAGE = eINSTANCE.getlegalName_Language();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.addressImpl <em>address</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.addressImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getaddress()
		 * @generated
		 */
		EClass ADDRESS = eINSTANCE.getaddress();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__LANGUAGE = eINSTANCE.getaddress_Language();

		/**
		 * The meta object literal for the '<em><b>Address Lines</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__ADDRESS_LINES = eINSTANCE.getaddress_AddressLines();

		/**
		 * The meta object literal for the '<em><b>Address Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__ADDRESS_NUMBER = eINSTANCE.getaddress_AddressNumber();

		/**
		 * The meta object literal for the '<em><b>Address Number Within Building</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__ADDRESS_NUMBER_WITHIN_BUILDING = eINSTANCE.getaddress_AddressNumberWithinBuilding();

		/**
		 * The meta object literal for the '<em><b>Mail Routing</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__MAIL_ROUTING = eINSTANCE.getaddress_MailRouting();

		/**
		 * The meta object literal for the '<em><b>City</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__CITY = eINSTANCE.getaddress_City();

		/**
		 * The meta object literal for the '<em><b>Region</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__REGION = eINSTANCE.getaddress_Region();

		/**
		 * The meta object literal for the '<em><b>Country</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__COUNTRY = eINSTANCE.getaddress_Country();

		/**
		 * The meta object literal for the '<em><b>Postal Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__POSTAL_CODE = eINSTANCE.getaddress_PostalCode();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.relatedEntityImpl <em>related Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.relatedEntityImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getrelatedEntity()
		 * @generated
		 */
		EClass RELATED_ENTITY = eINSTANCE.getrelatedEntity();

		/**
		 * The meta object literal for the '<em><b>Lei</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATED_ENTITY__LEI = eINSTANCE.getrelatedEntity_Lei();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATED_ENTITY__NAME = eINSTANCE.getrelatedEntity_Name();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.companyImpl <em>company</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.companyImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getcompany()
		 * @generated
		 */
		EClass COMPANY = eINSTANCE.getcompany();

		/**
		 * The meta object literal for the '<em><b>Legal Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPANY__LEGAL_NAME = eINSTANCE.getcompany_LegalName();

		/**
		 * The meta object literal for the '<em><b>Legal Address</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPANY__LEGAL_ADDRESS = eINSTANCE.getcompany_LegalAddress();

		/**
		 * The meta object literal for the '<em><b>Headquarters Address</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPANY__HEADQUARTERS_ADDRESS = eINSTANCE.getcompany_HeadquartersAddress();

		/**
		 * The meta object literal for the '<em><b>Registered As</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPANY__REGISTERED_AS = eINSTANCE.getcompany_RegisteredAs();

		/**
		 * The meta object literal for the '<em><b>Jurisdiction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPANY__JURISDICTION = eINSTANCE.getcompany_Jurisdiction();

		/**
		 * The meta object literal for the '<em><b>Associated Entity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPANY__ASSOCIATED_ENTITY = eINSTANCE.getcompany_AssociatedEntity();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPANY__STATUS = eINSTANCE.getcompany_Status();

		/**
		 * The meta object literal for the '<em><b>Successor Entity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPANY__SUCCESSOR_ENTITY = eINSTANCE.getcompany_SuccessorEntity();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.registrationImpl <em>registration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.registrationImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getregistration()
		 * @generated
		 */
		EClass REGISTRATION = eINSTANCE.getregistration();

		/**
		 * The meta object literal for the '<em><b>Initial Registration Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGISTRATION__INITIAL_REGISTRATION_DATE = eINSTANCE.getregistration_InitialRegistrationDate();

		/**
		 * The meta object literal for the '<em><b>Last Update Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGISTRATION__LAST_UPDATE_DATE = eINSTANCE.getregistration_LastUpdateDate();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGISTRATION__STATUS = eINSTANCE.getregistration_Status();

		/**
		 * The meta object literal for the '<em><b>Next Renewal Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGISTRATION__NEXT_RENEWAL_DATE = eINSTANCE.getregistration_NextRenewalDate();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.leiRecordImpl <em>lei Record</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.leiRecordImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getleiRecord()
		 * @generated
		 */
		EClass LEI_RECORD = eINSTANCE.getleiRecord();

		/**
		 * The meta object literal for the '<em><b>Lei</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEI_RECORD__LEI = eINSTANCE.getleiRecord_Lei();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEI_RECORD__ENTITY = eINSTANCE.getleiRecord_Entity();

		/**
		 * The meta object literal for the '<em><b>Registration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEI_RECORD__REGISTRATION = eINSTANCE.getleiRecord_Registration();

		/**
		 * The meta object literal for the '<em><b>Links</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEI_RECORD__LINKS = eINSTANCE.getleiRecord_Links();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.relationshipsImpl <em>relationships</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.relationshipsImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getrelationships()
		 * @generated
		 */
		EClass RELATIONSHIPS = eINSTANCE.getrelationships();

		/**
		 * The meta object literal for the '<em><b>Managing Lou</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIPS__MANAGING_LOU = eINSTANCE.getrelationships_ManagingLou();

		/**
		 * The meta object literal for the '<em><b>Lei Issuer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIPS__LEI_ISSUER = eINSTANCE.getrelationships_LeiIssuer();

		/**
		 * The meta object literal for the '<em><b>Direct Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIPS__DIRECT_PARENT = eINSTANCE.getrelationships_DirectParent();

		/**
		 * The meta object literal for the '<em><b>Ultimate Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIPS__ULTIMATE_PARENT = eINSTANCE.getrelationships_UltimateParent();

		/**
		 * The meta object literal for the '<em><b>Direct Children</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIPS__DIRECT_CHILDREN = eINSTANCE.getrelationships_DirectChildren();

		/**
		 * The meta object literal for the '{@link gleifmodel.impl.linksImpl <em>links</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.impl.linksImpl
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getlinks()
		 * @generated
		 */
		EClass LINKS = eINSTANCE.getlinks();

		/**
		 * The meta object literal for the '<em><b>Lei Record</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINKS__LEI_RECORD = eINSTANCE.getlinks_LeiRecord();

		/**
		 * The meta object literal for the '<em><b>Relationship Record</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINKS__RELATIONSHIP_RECORD = eINSTANCE.getlinks_RelationshipRecord();

		/**
		 * The meta object literal for the '{@link gleifmodel.entityStatus <em>entity Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.entityStatus
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getentityStatus()
		 * @generated
		 */
		EEnum ENTITY_STATUS = eINSTANCE.getentityStatus();

		/**
		 * The meta object literal for the '{@link gleifmodel.registartionStatus <em>registartion Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gleifmodel.registartionStatus
		 * @see gleifmodel.impl.GleifmodelPackageImpl#getregistartionStatus()
		 * @generated
		 */
		EEnum REGISTARTION_STATUS = eINSTANCE.getregistartionStatus();

	}

} //GleifmodelPackage
