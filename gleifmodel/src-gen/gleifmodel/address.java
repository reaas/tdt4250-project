/**
 */
package gleifmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>address</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.address#getLanguage <em>Language</em>}</li>
 *   <li>{@link gleifmodel.address#getAddressLines <em>Address Lines</em>}</li>
 *   <li>{@link gleifmodel.address#getAddressNumber <em>Address Number</em>}</li>
 *   <li>{@link gleifmodel.address#getAddressNumberWithinBuilding <em>Address Number Within Building</em>}</li>
 *   <li>{@link gleifmodel.address#getMailRouting <em>Mail Routing</em>}</li>
 *   <li>{@link gleifmodel.address#getCity <em>City</em>}</li>
 *   <li>{@link gleifmodel.address#getRegion <em>Region</em>}</li>
 *   <li>{@link gleifmodel.address#getCountry <em>Country</em>}</li>
 *   <li>{@link gleifmodel.address#getPostalCode <em>Postal Code</em>}</li>
 * </ul>
 *
 * @see gleifmodel.GleifmodelPackage#getaddress()
 * @model
 * @generated
 */
public interface address extends EObject {
	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_Language()
	 * @model
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Address Lines</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Lines</em>' attribute.
	 * @see #setAddressLines(EList)
	 * @see gleifmodel.GleifmodelPackage#getaddress_AddressLines()
	 * @model many="false" transient="true"
	 * @generated
	 */
	EList<?> getAddressLines();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getAddressLines <em>Address Lines</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address Lines</em>' attribute.
	 * @see #getAddressLines()
	 * @generated
	 */
	void setAddressLines(EList<?> value);

	/**
	 * Returns the value of the '<em><b>Address Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Number</em>' attribute.
	 * @see #setAddressNumber(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_AddressNumber()
	 * @model
	 * @generated
	 */
	String getAddressNumber();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getAddressNumber <em>Address Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address Number</em>' attribute.
	 * @see #getAddressNumber()
	 * @generated
	 */
	void setAddressNumber(String value);

	/**
	 * Returns the value of the '<em><b>Address Number Within Building</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Number Within Building</em>' attribute.
	 * @see #setAddressNumberWithinBuilding(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_AddressNumberWithinBuilding()
	 * @model
	 * @generated
	 */
	String getAddressNumberWithinBuilding();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getAddressNumberWithinBuilding <em>Address Number Within Building</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address Number Within Building</em>' attribute.
	 * @see #getAddressNumberWithinBuilding()
	 * @generated
	 */
	void setAddressNumberWithinBuilding(String value);

	/**
	 * Returns the value of the '<em><b>Mail Routing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mail Routing</em>' attribute.
	 * @see #setMailRouting(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_MailRouting()
	 * @model
	 * @generated
	 */
	String getMailRouting();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getMailRouting <em>Mail Routing</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mail Routing</em>' attribute.
	 * @see #getMailRouting()
	 * @generated
	 */
	void setMailRouting(String value);

	/**
	 * Returns the value of the '<em><b>City</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>City</em>' attribute.
	 * @see #setCity(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_City()
	 * @model
	 * @generated
	 */
	String getCity();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getCity <em>City</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>City</em>' attribute.
	 * @see #getCity()
	 * @generated
	 */
	void setCity(String value);

	/**
	 * Returns the value of the '<em><b>Region</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Region</em>' attribute.
	 * @see #setRegion(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_Region()
	 * @model
	 * @generated
	 */
	String getRegion();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getRegion <em>Region</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Region</em>' attribute.
	 * @see #getRegion()
	 * @generated
	 */
	void setRegion(String value);

	/**
	 * Returns the value of the '<em><b>Country</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Country</em>' attribute.
	 * @see #setCountry(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_Country()
	 * @model
	 * @generated
	 */
	String getCountry();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getCountry <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Country</em>' attribute.
	 * @see #getCountry()
	 * @generated
	 */
	void setCountry(String value);

	/**
	 * Returns the value of the '<em><b>Postal Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postal Code</em>' attribute.
	 * @see #setPostalCode(String)
	 * @see gleifmodel.GleifmodelPackage#getaddress_PostalCode()
	 * @model
	 * @generated
	 */
	String getPostalCode();

	/**
	 * Sets the value of the '{@link gleifmodel.address#getPostalCode <em>Postal Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postal Code</em>' attribute.
	 * @see #getPostalCode()
	 * @generated
	 */
	void setPostalCode(String value);

} // address
