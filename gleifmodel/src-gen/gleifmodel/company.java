/**
 */
package gleifmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>company</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gleifmodel.company#getLegalName <em>Legal Name</em>}</li>
 *   <li>{@link gleifmodel.company#getLegalAddress <em>Legal Address</em>}</li>
 *   <li>{@link gleifmodel.company#getHeadquartersAddress <em>Headquarters Address</em>}</li>
 *   <li>{@link gleifmodel.company#getRegisteredAs <em>Registered As</em>}</li>
 *   <li>{@link gleifmodel.company#getJurisdiction <em>Jurisdiction</em>}</li>
 *   <li>{@link gleifmodel.company#getAssociatedEntity <em>Associated Entity</em>}</li>
 *   <li>{@link gleifmodel.company#getStatus <em>Status</em>}</li>
 *   <li>{@link gleifmodel.company#getSuccessorEntity <em>Successor Entity</em>}</li>
 * </ul>
 *
 * @see gleifmodel.GleifmodelPackage#getcompany()
 * @model
 * @generated
 */
public interface company extends EObject {
	/**
	 * Returns the value of the '<em><b>Legal Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Legal Name</em>' reference.
	 * @see #setLegalName(legalName)
	 * @see gleifmodel.GleifmodelPackage#getcompany_LegalName()
	 * @model
	 * @generated
	 */
	legalName getLegalName();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getLegalName <em>Legal Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Legal Name</em>' reference.
	 * @see #getLegalName()
	 * @generated
	 */
	void setLegalName(legalName value);

	/**
	 * Returns the value of the '<em><b>Legal Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Legal Address</em>' reference.
	 * @see #setLegalAddress(address)
	 * @see gleifmodel.GleifmodelPackage#getcompany_LegalAddress()
	 * @model
	 * @generated
	 */
	address getLegalAddress();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getLegalAddress <em>Legal Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Legal Address</em>' reference.
	 * @see #getLegalAddress()
	 * @generated
	 */
	void setLegalAddress(address value);

	/**
	 * Returns the value of the '<em><b>Headquarters Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Headquarters Address</em>' reference.
	 * @see #setHeadquartersAddress(address)
	 * @see gleifmodel.GleifmodelPackage#getcompany_HeadquartersAddress()
	 * @model
	 * @generated
	 */
	address getHeadquartersAddress();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getHeadquartersAddress <em>Headquarters Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Headquarters Address</em>' reference.
	 * @see #getHeadquartersAddress()
	 * @generated
	 */
	void setHeadquartersAddress(address value);

	/**
	 * Returns the value of the '<em><b>Registered As</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered As</em>' attribute.
	 * @see #setRegisteredAs(String)
	 * @see gleifmodel.GleifmodelPackage#getcompany_RegisteredAs()
	 * @model
	 * @generated
	 */
	String getRegisteredAs();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getRegisteredAs <em>Registered As</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Registered As</em>' attribute.
	 * @see #getRegisteredAs()
	 * @generated
	 */
	void setRegisteredAs(String value);

	/**
	 * Returns the value of the '<em><b>Jurisdiction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jurisdiction</em>' attribute.
	 * @see #setJurisdiction(String)
	 * @see gleifmodel.GleifmodelPackage#getcompany_Jurisdiction()
	 * @model
	 * @generated
	 */
	String getJurisdiction();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getJurisdiction <em>Jurisdiction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jurisdiction</em>' attribute.
	 * @see #getJurisdiction()
	 * @generated
	 */
	void setJurisdiction(String value);

	/**
	 * Returns the value of the '<em><b>Associated Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Entity</em>' reference.
	 * @see #setAssociatedEntity(relatedEntity)
	 * @see gleifmodel.GleifmodelPackage#getcompany_AssociatedEntity()
	 * @model
	 * @generated
	 */
	relatedEntity getAssociatedEntity();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getAssociatedEntity <em>Associated Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Entity</em>' reference.
	 * @see #getAssociatedEntity()
	 * @generated
	 */
	void setAssociatedEntity(relatedEntity value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link gleifmodel.entityStatus}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see gleifmodel.entityStatus
	 * @see #setStatus(entityStatus)
	 * @see gleifmodel.GleifmodelPackage#getcompany_Status()
	 * @model
	 * @generated
	 */
	entityStatus getStatus();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see gleifmodel.entityStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(entityStatus value);

	/**
	 * Returns the value of the '<em><b>Successor Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Successor Entity</em>' reference.
	 * @see #setSuccessorEntity(relatedEntity)
	 * @see gleifmodel.GleifmodelPackage#getcompany_SuccessorEntity()
	 * @model
	 * @generated
	 */
	relatedEntity getSuccessorEntity();

	/**
	 * Sets the value of the '{@link gleifmodel.company#getSuccessorEntity <em>Successor Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Successor Entity</em>' reference.
	 * @see #getSuccessorEntity()
	 * @generated
	 */
	void setSuccessorEntity(relatedEntity value);

} // company
