# Gleifmodel
## About the model
The model is modeled after the GLEIF API specifications. The thought is that by transforming the responses from the API into its own model, is to make the data available to different platforms and applications.

### `gleif`
On the top level you have the `gleif` model. This entity contains a list of `leiRecord`s and these in turn are information about companies fetched from the GLEIF API.

### `leiRecord`
This record contains information about a company. Names, addresses, lei-number, relationships with other companies and more.
Each `leiRecord` contains references to possible other `leiRecords`, like for example Norwegian Bank is owned by Norwegian ASA, and this relationship is present in the model.

### `entity`
This is the base information within a `leiRecord`. This is where you'll find the basic information about each record.

### `registration`
This is contained in the `leiRecord` and consists of registration information about the company

### `links`
This is contained in the `leiRecord` and consists of links to other `leiRecord`s and their connection.

### `relationships`
This is contained in the `leiRecord` and consists of relationships with other `leiRecord`s, that being parent- or child companies and the like. By taking advantage of this one can easily follow the "chain of command" from a company.

### Misc
There are also a couple of Enums to keep easy track of statuses.