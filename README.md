# TDT4250 - Semester project
##### By Rolf Aas
Because of COVID-19 and the fact that I have never met anyone taking this course this semester, I decided to work alone. This, however, proved to be difficult as the scope became larger than first anticipated. I am, however, satisfied with my result and I feel I have learned a lot the last weeks.

## About my project
This repository contains my semester project for TDT4250 - Advanced Software Design.

You'll find all relevant code here, as well as detailed READMEs in each bundle. This README is an overview of the project and this repository.

My idea for this project was to model the LEI-numbers (Legal Entity Identifier) one get from sending requsts to the GLEIF (Global Legal Entity Identifier Foundation) API. These LEI-records contains all information about each company, their relationships with other companies (be it parent/child companies), as well as legal information such as names, addresses and more.

You can read more about LEIs [here](https://www.gleif.org/en/about-lei/introducing-the-legal-entity-identifier-lei).

I modeled the date using Ecore, and transformed this to text using Acceleo. I initially wanted to take advantage of EMF Forms, but after several weeks of trying and failing, I ended up coding the forms myself in HTML and JavaScript. I first tried to use the [RAP-renderer](https://www.eclipse.org/rap/) but this lead to many dependency issues. After fixing these issues, my Acceleo would not work as intended. I never got the skill or experience juggling dependencies between different bundles, and I feel this is my biggest weak point in this course.
After the RAP failure, I tried my hands on the [SWT-renderer](https://eclipsesource.com/blogs/tutorials/getting-started-with-EMF-Forms#swt) but ended up with the same problems as with the RAP-renderer. I wanted to make this in to an application to be run on the computer, but as I already had experience using Acceleo for the web, I ended up going this route.
You can, however, see my `.viewforms` [here](https://gitlab.stud.idi.ntnu.no/reaas/tdt4250-project/-/tree/master/gleifmodel.viewmodel/viewmodels) and my brave attempt on SWT [here](https://gitlab.stud.idi.ntnu.no/reaas/tdt4250-project/-/blob/master/gleifmodel.acceleo.web/src/gleifmodel/acceleo/web/view/View.java).

As neither the RAP- or the SWT-renderer worked, I ended up writing the from and form-handlers myself using HTML and JavaScript. This works, but I know this does not meet the criteria for the project. However, I feel that the model and Acceleo parts are solid.

For more information about each bundle, see their respective READMEs.

## Motivation
During my work on my groups project in the course [TDT4290 - Customer driven project](https://www.ntnu.edu/studies/courses/TDT4290#tab=omEmnet) we were tasked with developing a solution to easy the communication between departments in a Financial Assets Management firm. This lead us to discovering the GLEIF API, which intrigued me. The amount of data that was available and the fact that it was free to use made it suitable for this project as well. I wanted to make the data sets gathered from this API available as its own DSL, which in turn could be used in several different application, and on multiple platforms. This information is in use daily across the globe in many different sectors and therefore the data should be easily accessible.

## How to run
### Prerequisits
You need to have the latest versions of Java (8.0.261) and Eclipse (2020-06) (also works on 2020-09).

The following plug-ins must be installed:
From the standard software site (select Eclipse 2020-06 in drop-down) and with Group Items by Category checked

* **Acceleo** - model to text transformation (M2T) and OCL interpreter view
* **Ecore Diagram Editor (SDK)** - editor for ecore models as diagram
* **EMF Forms SDK** - forms for ecore models

From the standard software site (select Eclipse 2020-06 in drop-down) and with Group Items by Category un-checked:

* **Acceleo Query SDK** - OCL implementation

From the software site http://hallvard.github.io/plantuml (type into text field):

PlantUML Ecore Feature and PlantUML Feature (under PlantUML Eclipse support)
PlantUML Library Feature (under PlantUML Library)

In addition, install the `graphviz` command line application and register its path to dot executable in the PlantUML preferences in Eclipse.
The Eclipse PlantUML plugin is incompatible with the latest graphviz version, so use v2.38.

### Running
1. Clone the repo ([Link](https://gitlab.stud.idi.ntnu.no/reaas/tdt4250-project))
2. Then generate the required `.gleifmodel` file by running the `gleifmodel.instance.generator.InstanceGenerator` as a Java Application in Eclipse.
  * This creates a `.gleifmodel` inside `gleifmodel.instance.gleifmodel` folder. The `InstanceGenerator` fetches a list of LEI-numbers from the GLEIF API and creates a `.xmi` file out of the correct Ecore model. 
3. Run the `gleifmodel.acceleo.web.main.generate.mtl` as an Acceleo Application with the following configurations:
  * **Project**: `gleifmodel.acceleo.web`
  * **Main class**: `gleifmodel.acceleo.web.main.Generate`
  * **Model**: `/gleifmodel.instance/gleifmodel/gleif.gleifmodel`
  * **Target**: `/gleifmodel.acceleo.web/src/gleifmodel/acceleo/web/main`
  * **Runnder**: `Java Application`
4. This has generated three files:
  * `gleifmodel.acceleo.web.main.www.index.html` - The HTML file
  * `gleifmodel.acceleo.web.main.www.styles.index.css` - The CSS file
  * `gleifmodel.acceleo.web.main.www.scripts.index.js`- The script file
5. Open the `gleifmodel.acceleo.web.main.www.index.html` file. Preferably in Firefox or Chrome as the built-in web browser in Eclipse has varying support for CSS and JavaScript.

### Notes
#### Acceleo
To be able to use the `GleifmodelPackage Registry` I had to add this package to the `registerPackages`-method in the `gleifmodel.acceleo.web.main.Generate.java`-file. This such that Acceleo could import the correct package.

#### InstanceGenerator
This is a custom built instance generator for the Gleifmodel. It fetches data from the GLEIF API, parses the JSON response and creates intances of the LeiRecord model. 

#### XMIGenerator
I wrote this to build an `.xmi` file out of the instance created by the `InstanceGenerator`. This was before I discovered that the `org.eclipse.emf.ecore.resource.Resource.save()` method actually saves a file to the computer. I first believed it just created the instance in the memory. This file is therefor redundant, but I am pretty satisfied with the result so I decided to leave it be.

### Going forward
As I do not have enough experience with this kind of development, or enough time to continue, I have not delivered a 100% working solution. I am satisfied with the implementations I have done but there is a lot of work left to do. If I were to further develop this solution I would:

* Add persistant storage of changes made. Possibly by using SQLite or similar storage solutions
* Embed EMF-forms to make extension easy and maintenence a breeze
  * By doing this it would be possible to have support for both applications and web at the same time
* Expand the model to include proper nested `leyRecord`s for faster look up times and customizability
