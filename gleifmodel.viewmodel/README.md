# Viewmodel
This was my attempt to implement EMF-forms in my solution. The forms are correctly created and styled as I would want, but I did not manage to embed them in my application. This is discussed in the main `README.md` in the root directory.

Each part of the model has its own `viewmodel` and I have tried to replicate the layout and functionality in HTML in the final result.