# Instance
This bundle is my custom made instance generator for the `Gleifmodel`. 

## `DataFetcher.java`
The data from the GLEIF API is fetched and pre-processed here. 

## `InstanceGenerator.java`
The data fetched by the `DataFetcher` is processed here. By parsing JSON into the `Gleifmodel` DSL, we generate correct instances of the model. After all responses are processed, a `gleif.gleifmodel` file is generated, which is to be used when transforming the model to text using Acceleo.

## `InstanceLoader.java`
This was made to load the instance and parse it to `.xmi`. This, alongside `XMIGenerator`, was developed before I noticed that `org.eclipse.emf.ecore.resource.Resource.save()` actually created a `xmi` styled document.

## `XMIGenerator.java`
Made before I noticed that `org.eclipse.emf.ecore.resource.Resource.save()` created a `xmi` styled document. I wanted to use XMI when transforming from model to text with Acceleo, and such wrote this. I have left it in as I am satisfied with what I created and I could possible have use for it later in my studies.