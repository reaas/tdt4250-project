package gleifmodel.instance.generator;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import gleifmodel.gleifmodel.GleifmodelPackage;
import gleifmodel.gleifmodel.gleif;

public class InstanceLoader {
	public gleif loadInstance() {
		GleifmodelPackage.eINSTANCE.eClass();
		
		Resource.Factory.Registry registry = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> extensionMap = registry.getExtensionToFactoryMap();
		extensionMap.put("gleifmodel", new XMIResourceFactoryImpl());
		
		ResourceSet resSet = new ResourceSetImpl();

        Resource resource = resSet.getResource(URI
                .createURI("gleifmodel/gleif.gleifmodel"), true);
        
        gleif gleif = (gleif) resource.getContents().get(resource.getContents().size() - 1);
        return gleif;
	}
}
