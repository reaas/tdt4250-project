package gleifmodel.instance.generator;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class DataFetcher {
	private HttpClient client;
	
	public DataFetcher() {
		this.client = HttpClient.newHttpClient();
	}
	
	public String fetchData(String url) {
		if (url == null) {
			return "{\"error\": \"true\", \"errorMessage\": \"URL was null\"}";
		}
		URI uri = URI.create(url);
		HttpRequest request = HttpRequest.newBuilder(uri).build();
		
		String responseString;
		try {
			HttpResponse<String> response = this.client.send(request, BodyHandlers.ofString());
			
			responseString = response.body();
		} catch (IOException | InterruptedException e) {
			responseString = "{'error': 'true', 'errorMessage': 'Something went wrong with the HTTP request'}";
			e.printStackTrace();
		}
		
		return responseString;
	}
}
