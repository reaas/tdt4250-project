package gleifmodel.instance.generator;

// import java.io.File;
// import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.common.util.BasicEList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import gleifmodel.gleifmodel.GleifmodelFactory;
import gleifmodel.gleifmodel.GleifmodelPackage;
import gleifmodel.gleifmodel.address;
import gleifmodel.gleifmodel.company;
import gleifmodel.gleifmodel.entityStatus;
import gleifmodel.gleifmodel.gleif;
import gleifmodel.gleifmodel.legalName;
import gleifmodel.gleifmodel.leiRecord;
import gleifmodel.gleifmodel.links;
import gleifmodel.gleifmodel.registartionStatus;
import gleifmodel.gleifmodel.registration;
import gleifmodel.gleifmodel.relatedEntity;
import gleifmodel.gleifmodel.relationships;


public class InstanceGenerator {
	public static int depthCounter = 0;
	final static public int MAX_DEPTH = 10;
	
	final static DataFetcher df = new DataFetcher();
	
	final static List<String> leis = new ArrayList<String>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			add("5967007LIEEXZX6ZCW47");
			add("529900W18LQJJN6SJ336");
			add("254900PS6TE65C9V4D71");
			add("969500UHSOHD9X46JB49");
			add("5299000W0HB7SX4P4882");
		}
	};
	
	static List<leiRecord> leiRecords = new ArrayList<leiRecord>();
	
	/**
	 * This main method builds and instance of the gleifmodel.
	 * 
	 * First data is fetched from the GLEIF API using the LEI-numbers
	 * stated in the leis array.
	 * 
	 * Second it parses the JSON response.
	 * 
	 * Third an XMI file is built using the parsed JSON
	 * 
	 * Lastly the XMI file is saved to be used in Acceleo
	 * @param args
	 */
	public static void main(String[] args) {
		GleifmodelPackage.eINSTANCE.eClass();
		GleifmodelFactory factory = GleifmodelFactory.eINSTANCE;
		
		// Building the required objects
		for (String lei : leis) {
			depthCounter = 0;
			String response = df.fetchData("https://api.gleif.org/api/v1/lei-records/" + lei);
			
			try {
				Object parsedResponse = new JSONParser().parse(response);
				
				JSONObject responseJSON = (JSONObject) parsedResponse;
				
				if (responseJSON.containsKey("error")) {
					// System.err.println("Error: " + responseJSON.get("errorMessage"));
				} else {
					leiRecord record = buildInstance(factory, responseJSON);
					leiRecords.add(record);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		gleif parent = factory.creategleif();
		
		// Saving the collection to be extracted by the XMI generator
		Resource.Factory.Registry registry = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> extensionMap = registry.getExtensionToFactoryMap();
		extensionMap.put("gleifmodel", new XMIResourceFactoryImpl());
		
		ResourceSet resourceSet = new ResourceSetImpl();
		
		Resource resource = resourceSet.createResource(URI.createURI("gleifmodel/gleif.gleifmodel"));
		
		for (leiRecord record : leiRecords) {
			parent.getRecords().add(record);
			resource.getContents().add(record);
			resource.getContents().add(record.getEntity());
			resource.getContents().add(record.getEntity().getLegalName());
			resource.getContents().add(record.getEntity().getLegalAddress());
			resource.getContents().add(record.getEntity().getHeadquartersAddress());
			resource.getContents().add(record.getEntity().getAssociatedEntity());
			resource.getContents().add(record.getEntity().getSuccessorEntity());
			
			resource.getContents().add(record.getRegistration());
			resource.getContents().add(record.getRelationships());
			resource.getContents().add(record.getRelationships().getDirectParent());
			resource.getContents().add(record.getRelationships().getUltimateParent());
			
			if (record.getLinks() != null) {
				resource.getContents().add(record.getLinks());
			}
		}
		resource.getContents().add(parent);
		
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/**
		 * Now that the model is created with live data from the GLEIF API
		 * we can create an XMI file to be used in Acceleo.
		 * 
		 * It was discovered late in the development phase that this is indeed
		 * redundant. The above code actually generates a usable .gleifmodel XMI file
		 * for Accele to use.
		 */
//		XMIGenerator generator = new XMIGenerator();
//		generator.init();
//		
//		String gleifXMI = generator.generateXMI();
//		
//		try {
//			File gleifXMIFile = new File("gleifmodel.xmi");
//			
//			Boolean fileOK = false;
//			
//			if (!gleifXMIFile.exists()) {
//				if (!gleifXMIFile.createNewFile()) {
//					System.err.println("Error creating file: " + gleifXMIFile.getName());
//				} else {
//					fileOK = true;
//				}
//			} else {
//				fileOK = true;
//			}
//			
//			if (fileOK) {
//				FileWriter writer = new FileWriter("gleifmodel.xmi");
//				
//				writer.write(gleifXMI);
//				writer.close();
//				
//				System.out.println("gleifmodel.xmi created in " + gleifXMIFile.getAbsolutePath());
//			} else {
//				System.err.println("Something is wrong with file gleifmodel.xmi");
//			}
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
	}
	
	/**
	 * Builds a leiRecord object by parsing the JSON fetched from the GLEIF API
	 * @param factory The model factory to be used
	 * @param obj The JSON response from the API
	 * @return leiRecord object
	 */
	private static leiRecord buildInstance(GleifmodelFactory factory, JSONObject obj) {
		depthCounter++;
		
		leiRecord record = factory.createleiRecord();
		
		JSONObject jData = (JSONObject) obj.get("data");		
		JSONObject jAttributes = (JSONObject) jData.get("attributes");

		record.setLei((String) jAttributes.get("lei"));
		
		JSONObject jCompany = (JSONObject) jAttributes.get("entity");
		company company = buildCompany(factory, jCompany);
		record.setEntity(company);
		
		JSONObject jRegistration = (JSONObject) jAttributes.get("registration");
		registration registration = buildRegistration(factory, jRegistration);
		record.setRegistration(registration);
		
		JSONObject jRelationships = (JSONObject) jData.get("relationships");
		relationships relationships = buildRelationships(factory, jRelationships);
		record.setRelationships(relationships);
		
		return record;
	}
	
	/**
	 * Builds an entity object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jCompany The entity JSON from the API
	 * @return company object
	 */
	private static company buildCompany(GleifmodelFactory factory, JSONObject jCompany) {
		company company = factory.createcompany();
		
		company.setLegalName(buildLegalName(factory, (JSONObject) jCompany.get("legalName")));
		company.setLegalAddress(buildAddress(factory, (JSONObject) jCompany.get("legalAddress")));
		company.setHeadquartersAddress(buildAddress(factory, (JSONObject) jCompany.get("headquartersAddress")));
		
		company.setRegisteredAs((String) jCompany.get("registreredAs"));
		company.setJurisdiction((String) jCompany.get("jurisdiction"));
		
		company.setAssociatedEntity(buildRelatedEntity(factory, (JSONObject) jCompany.get("associatedEntity")));
		
		company.setStatus(getStatus((String) jCompany.get("status")));
		
		company.setSuccessorEntity(buildRelatedEntity(factory, (JSONObject) jCompany.get("successorEntity")));
		
		return company;
	}
	
	/**
	 * Builds a legalName object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jLegalName The legalName JSON from the API
	 * @return legalName object
	 */
	private static legalName buildLegalName(GleifmodelFactory factory, JSONObject jLegalName) {
		legalName legalName = factory.createlegalName();
		
		legalName.setName((String) jLegalName.get("name"));
		legalName.setLanguage((String) jLegalName.get("language"));
		
		return legalName;
	}
	
	/**
	 * Builds an address object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jAddress The address JSON from the API
	 * @return address object
	 */
	private static address buildAddress(GleifmodelFactory factory, JSONObject jAddress) {
		address address = factory.createaddress();
		
		address.setLanguage((String) jAddress.get("language"));
		address.setAddressNumber((String) jAddress.get("addressNumber"));
		address.setAddressNumberWithinBuilding((String) jAddress.get("addressNumberWithinBuilding"));
		address.setMailRouting((String) jAddress.get("mailRouting"));
		address.setCity((String) jAddress.get("city"));
		address.setRegion((String) jAddress.get("city"));
		address.setCountry((String) jAddress.get("country"));
		address.setPostalCode((String) jAddress.get("postalCode"));
		
		JSONArray jAddressLines = (JSONArray) jAddress.get("addressLines");	
		EList<String> addressLines = new BasicEList<String>();
		
		for(Object line : jAddressLines) {
			addressLines.add((String) line);
		}
		
		address.setAddressLines(addressLines);
		
		return address;
	}
	
	/**
	 * Builds an relatedEntity object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jRelatedEntity The relatedEntity JSON from the API
	 * @return relatedEntity object
	 */
	private static relatedEntity buildRelatedEntity(GleifmodelFactory factory, JSONObject jRelatedEntity) {
		relatedEntity relatedEntity = factory.createrelatedEntity();
		
		relatedEntity.setLei((String) jRelatedEntity.get("lei"));
		relatedEntity.setName((String) jRelatedEntity.get("name"));
		
		return relatedEntity;
	}
	
	/**
	 * Parses the status string to the entityStatus enum
	 * @param status
	 * @return entityStatus enum
	 */
	private static entityStatus getStatus(String status) {
		if (status.toLowerCase().equals("active")) {
			return entityStatus.ACTIVE;
		} else {
			return entityStatus.INACTIVE;
		}
	}
	
	/**
	 * Builds a registration object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jRegistration registration JSON from the API
	 * @return registration object
	 */
	private static registration buildRegistration(GleifmodelFactory factory, JSONObject jRegistration) {
		registration registration = factory.createregistration();
		
		try {
			registration.setInitialRegistrationDate(new SimpleDateFormat("yyyy-MM-dd").parse(jRegistration.get("initialRegistrationDate").toString().substring(0, 10)));
			registration.setLastUpdateDate(new SimpleDateFormat("yyyy-MM-dd").parse(jRegistration.get("lastUpdateDate").toString().substring(0, 10)));
			registration.setNextRenewalDate(new SimpleDateFormat("yyyy-MM-dd").parse(jRegistration.get("nextRenewalDate").toString().substring(0, 10)));
		} catch (java.text.ParseException e) {
			System.err.println("Error parsing date: " + jRegistration.get("initialRegistrationDate").toString().substring(0, 10));
			e.printStackTrace();
		}
		
		registration.setStatus(getRegistrationStatus((String) jRegistration.get("status")));
		
		return registration;
	}
	
	/**
	 * Parses the registrationStatus string to the registrationStatus enum
	 * @param status
	 * @return registrationStatus enum
	 */
	private static registartionStatus getRegistrationStatus(String status) {
		if (status.toLowerCase().equals("ISSUED")) {
			return registartionStatus.ISSUED;
		} else {
			return null;
		}
	}
	
	/**
	 * Builds a relationships object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jRelationships relationships JSON from the API
	 * @return relationships object
	 */
	private static relationships buildRelationships(GleifmodelFactory factory, JSONObject jRelationships) {
		relationships relationships = factory.createrelationships();
		
		JSONObject directParent = (JSONObject) jRelationships.get("direct-parent");
		JSONObject ultimateParent = (JSONObject) jRelationships.get("ultimate-parent");
		
		relationships.setDirectParent(buildLink(factory, (JSONObject) directParent.get("links")));
		relationships.setUltimateParent(buildLink(factory, (JSONObject) ultimateParent.get("links")));
		
		return relationships;
	}
	
	/**
	 * Builds a links object by parsing JSON
	 * @param factory The model factory to be used
	 * @param jLinks links JSON from the API
	 * @return links object
	 */
	private static links buildLink(GleifmodelFactory factory, JSONObject jLinks) {
		links link = factory.createlinks();

		String leiRecordResponse = df.fetchData((String) jLinks.get("lei-record"));
		
		try {
			Object parsedLeiRecordResponse = new JSONParser().parse(leiRecordResponse);
			
			JSONObject leiRecordResponseJSON = (JSONObject) parsedLeiRecordResponse;
			
			if (leiRecordResponseJSON.containsKey("error")) {
				// System.err.println("Error: " + leiRecordResponseJSON.get("errorMessage"));
			} else if (depthCounter > MAX_DEPTH) {
				return null;
			} else {
				leiRecord record = buildInstance(factory, leiRecordResponseJSON); 
				link.setLeiRecord(record);
				leiRecords.add(record);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return link;
	}
}
