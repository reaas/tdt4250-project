package gleifmodel.instance.generator;

import gleifmodel.gleifmodel.leiRecord;
import gleifmodel.gleifmodel.links;
import gleifmodel.gleifmodel.registration;
import gleifmodel.gleifmodel.relatedEntity;
import gleifmodel.gleifmodel.relationships;
import gleifmodel.gleifmodel.address;
import gleifmodel.gleifmodel.company;
import gleifmodel.gleifmodel.gleif;
import gleifmodel.gleifmodel.legalName;

/**
 * This class was redundant as the ResourceSet generates an XMI...
 * @author rolfaas
 *
 */
public class XMIGenerator {
	gleif gleif;
	public void init() {
		InstanceLoader loader = new InstanceLoader();
		
		this.gleif = loader.loadInstance();
	}
	
	public String generateXMI() {
		String gleifXMI = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		gleifXMI += "<gleifmodel:gleifmodel xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
				+ " xmlns:gleifmodel=\"http://www.example.org/gleifmodel\" xsi:schemaLocation=\"http://www.example.org/gleifmodel gleifmodel.ecore\" name=\"Gleifmodel\">\n";
		
		for (leiRecord record : gleif.getRecords()) {
			gleifXMI += "\t" + generateLeiRecordXMI(record) + "\n";
		}
		
		gleifXMI += "</gleifmodel:gleifmodel>";
		
		return gleifXMI;
	}
	
	private String generateLeiRecordXMI(leiRecord record) {
		String leiRecordXMI = "<leiRecord lei=\"" + record.getLei() + "\">\n";
		
		leiRecordXMI += "\t\t" + generateCompanyXMI(record.getEntity()) + "\n";
		leiRecordXMI += "\t\t" + generateRegistrationXMI(record.getRegistration()) + "\n";
		leiRecordXMI += "\t\t" + generateLinksXMI(record.getLinks()) + "\n";
		leiRecordXMI += "\t\t" + generateRelationShipsXMI(record.getRelationships()) + "\n";
		
		leiRecordXMI += "\t</leiRecord>";
		return leiRecordXMI;
	}
	
	private String generateCompanyXMI(company comp) {
		String companyXMI = "<entity registratedAs=\"" + comp.getRegisteredAs() + "\" ";
		companyXMI += "jurisdiction=\"" + comp.getJurisdiction() + "\" ";
		companyXMI += "status=\"" + comp.getStatus().getName() + "\" ";
		companyXMI += ">\n";
		
		companyXMI += "\t\t\t" + generateLegalNameXMI(comp.getLegalName()) + "\n";
		companyXMI += "\t\t\t" + generateAddressXMI("legal", comp.getLegalAddress()) + "\n";
		companyXMI += "\t\t\t" + generateAddressXMI("headquarters", comp.getHeadquartersAddress()) + "\n";
		companyXMI += "\t\t\t" + generateRelatedEntityXMI("associated", comp.getAssociatedEntity()) + "\n";
		companyXMI += "\t\t\t" + generateRelatedEntityXMI("successor", comp.getSuccessorEntity()) + "\n";
		
		companyXMI += "\t\t</entity>";
		return companyXMI;
	}
	
	private String generateLegalNameXMI(legalName name) {
		String legalNameXMI = "<legalName name=\"" + name.getName() + "\" language=\"" + name.getLanguage() + "\"></legalName>";
		
		return legalNameXMI;
	}
	
	private String generateAddressXMI(String type, address addr) {
		String addressXMI = "<" + type + "Address ";
		
		addressXMI += "langauge=\"" + addr.getLanguage() + "\" ";
		
		if (addr.getAddressLines() != null) {
			addressXMI += "addressLines=\"";
			for (Object line : addr.getAddressLines()) {
				addressXMI += line.toString() + " ";
			}
			addressXMI += "\" ";
		} else {
			addressXMI += "addressLines=\"null\" ";
		}
		
		addressXMI += "addressNumber=\"" + addr.getAddressNumber() + "\" ";
		addressXMI += "addressNumberWithinBuilding=\"" + addr.getAddressNumberWithinBuilding() + "\" ";
		addressXMI += "mailRouting=\"" + addr.getMailRouting() + "\" ";
		addressXMI += "city=\"" + addr.getCity() + "\" ";
		addressXMI += "region=\"" + addr.getRegion() + "\" ";
		addressXMI += "country=\"" + addr.getCountry() + "\" ";
		addressXMI += "postalCode=\"" + addr.getPostalCode() + "\"";
		
		addressXMI += "></" + type + "Address>";
		
		return addressXMI;
	}
	
	private String generateRelatedEntityXMI(String type, relatedEntity entity) {
		String relatedEntityXMI = "<" + type + "Entity ";
		
		relatedEntityXMI += "lei=\"" + entity.getLei() + "\" ";
		relatedEntityXMI += "name=\"" + entity.getName() + "\"";
		relatedEntityXMI += "></" + type + "Entity>";
		
		return relatedEntityXMI;
	}
	
	private String generateRegistrationXMI(registration reg) {
		String regXMI = "<registration ";
		
		regXMI += "initialRegistrationDate=\"" + reg.getInitialRegistrationDate().toString() + "\" ";
		regXMI += "lastUpdateDate=\"" + reg.getLastUpdateDate().toString() + "\" ";
		regXMI += "status=\"" + reg.getStatus().getName() + "\" ";
		regXMI += "nextRenewalDate=\"" + reg.getNextRenewalDate().toString() + "\"";
		
		regXMI += "></registration>";
		
		return regXMI;
	}
	
	private String generateLinksXMI(links link) {
		String linksXMI = "";
		
		if (link != null) {
			linksXMI += "<links>\n";
			if (link.getLeiRecord() != null) {
				linksXMI += "\t" + generateLeiRecordXMI(link.getLeiRecord()) + "\n";	
			}
			
			if (link.getRelationshipRecord() != null) {
				linksXMI += "\t" + generateLeiRecordXMI(link.getRelationshipRecord()) + "\n";	
			}
			linksXMI += "</links>";
		} else {
			linksXMI += "<links></links>";
		}
		
		return linksXMI;
	}
	
	private String generateRelationShipsXMI(relationships rel) {
		int found = 0;
		String relationshipXMI = "<relationships>";
		
		if (rel.getDirectParent() != null) {
			found++;
			relationshipXMI += "\n";
			relationshipXMI += "\t" + generateLinksXMI(rel.getDirectParent());
		}
		
		if (rel.getUltimateParent() != null) {
			found++;
			relationshipXMI += "\n";
			relationshipXMI += "\t" + generateLinksXMI(rel.getUltimateParent());
		}
		
		if (found > 0) {
			relationshipXMI += "\n</relationships>";
		} else {
			relationshipXMI += "</relationships>";
		}
		
		
		return relationshipXMI;
	}
	
	public static void main(String[] args) {
		XMIGenerator generator = new XMIGenerator();
		generator.init();
		generator.generateXMI();
	}
}
