# Acceleo
This bundle is the Acceleo model to text code. The `generate.mtl` file contains the HTML, CSS and JavaScript details to be generated, and the `Generate.java` file is the acctual generator class. This file has a small edit by be. The `GleifmodelPackage` had to manually be loaded for the transformation to work. Therefore, the `registerPackage()` method does not automatically re-generates when starting a new instance.

After running the `generate.mtl` with the specifications listed in the main `README.md`, three files will be generated:
1. `main/www/index.html`
2. `main/www/styles/index.css`
3. `main/www/scripts/index.js`

These are the result of this project.

The file `view/View.java` is my brave attempt on using the SWT-renderer to embed my EMF-forms in the application. This did not work as intended, and is described in the main `README.md`.