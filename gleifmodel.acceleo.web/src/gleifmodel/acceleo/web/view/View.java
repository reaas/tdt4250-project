package gleifmodel.acceleo.web.view;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecp.ui.view.ECPRendererException;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTViewRenderer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import gleifmodel.gleifmodel.GleifmodelPackage;
import gleifmodel.instance.generator.InstanceLoader;

public class View {
	public static void main(String[] args) {
		View view = new View();
		
		view.init();
	}
	
	public void init() {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		RowLayout layout = new RowLayout();
		layout.wrap= true;
		
		shell.setLayout(layout);
		
		createComposite(shell);
		
		shell.pack();
		shell.open();
		
		while(!shell.isDisposed()) {
			if (!display.readAndDispatch()) display.sleep();
		}
	}
	
	private EObject getGleifModel() {
		// final EClass eClass = GleifmodelPackage.eINSTANCE.getgleif();
		
		final InstanceLoader loader = new InstanceLoader();
		final EClass eClass = (EClass) loader.loadInstance();
		
		return EcoreUtil.create(eClass);
	}
	
	public void createComposite(Composite parent) {
		final EObject gleifmodel = getGleifModel();
		
		try {
			final Composite content = new Composite(parent, SWT.NONE);
			content.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			
			ECPSWTViewRenderer.INSTANCE.render(content, gleifmodel);
			
			content.layout();
		} catch (final ECPRendererException e) {
			e.printStackTrace();
		}
		parent.layout();
	}
}
