window.onload = function() {
	    document.getElementById("selection-5967007LIEEXZX70WG48").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "NORWEGIAN FINANS HOLDING ASA";

			// Registration
			const regDate = convertDate("Mon Nov 03 00:00:00 CET 2014");
			const lastUpdateDate = convertDate("Sun Nov 01 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Tue Oct 26 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "5967007LIEEXZX70WG48";

			// Company left
			document.getElementById("company-left-legal-name").value = "NORWEGIAN FINANS HOLDING ASA";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "FORNEBU";
			document.getElementById("company-left-legal-region").value = "FORNEBU";
			document.getElementById("company-left-legal-postal-code").value = "1364";

			document.getElementById("company-left-jurisdiction").value = "NO";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Fornebu";
			document.getElementById("company-right-headquarters-region").value = "Fornebu";
			document.getElementById("company-right-headquarters-postal-code").value = "1364";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-5967007LIEEXZX70WG48").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "NORWEGIAN FINANS HOLDING ASA";

			// Registration
			const regDate = convertDate("Mon Nov 03 00:00:00 CET 2014");
			const lastUpdateDate = convertDate("Sun Nov 01 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Tue Oct 26 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "5967007LIEEXZX70WG48";

			// Company left
			document.getElementById("company-left-legal-name").value = "NORWEGIAN FINANS HOLDING ASA";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "FORNEBU";
			document.getElementById("company-left-legal-region").value = "FORNEBU";
			document.getElementById("company-left-legal-postal-code").value = "1364";

			document.getElementById("company-left-jurisdiction").value = "NO";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Fornebu";
			document.getElementById("company-right-headquarters-region").value = "Fornebu";
			document.getElementById("company-right-headquarters-postal-code").value = "1364";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-5967007LIEEXZX6ZCW47").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "BANK NORWEGIAN AS";

			// Registration
			const regDate = convertDate("Mon Nov 03 00:00:00 CET 2014");
			const lastUpdateDate = convertDate("Mon Oct 26 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Tue Oct 26 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "5967007LIEEXZX6ZCW47";

			// Company left
			document.getElementById("company-left-legal-name").value = "BANK NORWEGIAN AS";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "FORNEBU";
			document.getElementById("company-left-legal-region").value = "FORNEBU";
			document.getElementById("company-left-legal-postal-code").value = "1364";

			document.getElementById("company-left-jurisdiction").value = "NO";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Fornebu";
			document.getElementById("company-right-headquarters-region").value = "Fornebu";
			document.getElementById("company-right-headquarters-postal-code").value = "1364";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-O2RNE8IBXP4R0TD8PU41").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "SOCIETE GENERALE";

			// Registration
			const regDate = convertDate("Tue Jun 05 00:00:00 CEST 2012");
			const lastUpdateDate = convertDate("Thu Dec 03 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Mon Jan 04 00:00:00 CET 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "O2RNE8IBXP4R0TD8PU41";

			// Company left
			document.getElementById("company-left-legal-name").value = "SOCIETE GENERALE";
			document.getElementById("company-left-legal-language").value = "fr";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "PARIS 9";
			document.getElementById("company-left-legal-region").value = "PARIS 9";
			document.getElementById("company-left-legal-postal-code").value = "75009";

			document.getElementById("company-left-jurisdiction").value = "FR";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "PARIS 9";
			document.getElementById("company-right-headquarters-region").value = "PARIS 9";
			document.getElementById("company-right-headquarters-postal-code").value = "75009";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-O2RNE8IBXP4R0TD8PU41").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "SOCIETE GENERALE";

			// Registration
			const regDate = convertDate("Tue Jun 05 00:00:00 CEST 2012");
			const lastUpdateDate = convertDate("Thu Dec 03 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Mon Jan 04 00:00:00 CET 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "O2RNE8IBXP4R0TD8PU41";

			// Company left
			document.getElementById("company-left-legal-name").value = "SOCIETE GENERALE";
			document.getElementById("company-left-legal-language").value = "fr";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "PARIS 9";
			document.getElementById("company-left-legal-region").value = "PARIS 9";
			document.getElementById("company-left-legal-postal-code").value = "75009";

			document.getElementById("company-left-jurisdiction").value = "FR";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "PARIS 9";
			document.getElementById("company-right-headquarters-region").value = "PARIS 9";
			document.getElementById("company-right-headquarters-postal-code").value = "75009";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-529900W18LQJJN6SJ336").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "Société Générale Effekten GmbH";

			// Registration
			const regDate = convertDate("Mon Jan 27 00:00:00 CET 2014");
			const lastUpdateDate = convertDate("Fri Oct 02 00:00:00 CEST 2020");
			const nextRenewalDate = convertDate("Sun Oct 17 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "529900W18LQJJN6SJ336";

			// Company left
			document.getElementById("company-left-legal-name").value = "Société Générale Effekten GmbH";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "Frankfurt am Main";
			document.getElementById("company-left-legal-region").value = "Frankfurt am Main";
			document.getElementById("company-left-legal-postal-code").value = "60311";

			document.getElementById("company-left-jurisdiction").value = "DE";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Frankfurt am Main";
			document.getElementById("company-right-headquarters-region").value = "Frankfurt am Main";
			document.getElementById("company-right-headquarters-postal-code").value = "60311";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-254900PS6TE65C9V4D71").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "Komplett AS";

			// Registration
			const regDate = convertDate("Thu Oct 26 00:00:00 CEST 2017");
			const lastUpdateDate = convertDate("Fri Oct 30 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Thu Oct 29 00:00:00 CET 2020");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "254900PS6TE65C9V4D71";

			// Company left
			document.getElementById("company-left-legal-name").value = "Komplett AS";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "Sandefjord";
			document.getElementById("company-left-legal-region").value = "Sandefjord";
			document.getElementById("company-left-legal-postal-code").value = "3241";

			document.getElementById("company-left-jurisdiction").value = "NO";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Sandefjord";
			document.getElementById("company-right-headquarters-region").value = "Sandefjord";
			document.getElementById("company-right-headquarters-postal-code").value = "3241";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-969500UHSOHD9X46JB49").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "VOLVO GROUP";

			// Registration
			const regDate = convertDate("Wed Jan 29 00:00:00 CET 2014");
			const lastUpdateDate = convertDate("Fri Mar 20 00:00:00 CET 2020");
			const nextRenewalDate = convertDate("Tue Jan 29 00:00:00 CET 2019");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "969500UHSOHD9X46JB49";

			// Company left
			document.getElementById("company-left-legal-name").value = "VOLVO GROUP";
			document.getElementById("company-left-legal-language").value = "fr";
			document.getElementById("company-left-status").value = "INACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "MALAKOFF";
			document.getElementById("company-left-legal-region").value = "MALAKOFF";
			document.getElementById("company-left-legal-postal-code").value = "92240";

			document.getElementById("company-left-jurisdiction").value = "FR";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "9695003C7PPLQN1DJN65";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "MALAKOFF";
			document.getElementById("company-right-headquarters-region").value = "MALAKOFF";
			document.getElementById("company-right-headquarters-postal-code").value = "92240";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-529900R27DL06UVNT076").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "Daimler AG";

			// Registration
			const regDate = convertDate("Fri Sep 27 00:00:00 CEST 2013");
			const lastUpdateDate = convertDate("Fri Aug 28 00:00:00 CEST 2020");
			const nextRenewalDate = convertDate("Mon Oct 25 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "529900R27DL06UVNT076";

			// Company left
			document.getElementById("company-left-legal-name").value = "Daimler AG";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "Stuttgart";
			document.getElementById("company-left-legal-region").value = "Stuttgart";
			document.getElementById("company-left-legal-postal-code").value = "70372";

			document.getElementById("company-left-jurisdiction").value = "DE";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Stuttgart";
			document.getElementById("company-right-headquarters-region").value = "Stuttgart";
			document.getElementById("company-right-headquarters-postal-code").value = "70372";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-529900R27DL06UVNT076").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "Daimler AG";

			// Registration
			const regDate = convertDate("Fri Sep 27 00:00:00 CEST 2013");
			const lastUpdateDate = convertDate("Fri Aug 28 00:00:00 CEST 2020");
			const nextRenewalDate = convertDate("Mon Oct 25 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "529900R27DL06UVNT076";

			// Company left
			document.getElementById("company-left-legal-name").value = "Daimler AG";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "Stuttgart";
			document.getElementById("company-left-legal-region").value = "Stuttgart";
			document.getElementById("company-left-legal-postal-code").value = "70372";

			document.getElementById("company-left-jurisdiction").value = "DE";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Stuttgart";
			document.getElementById("company-right-headquarters-region").value = "Stuttgart";
			document.getElementById("company-right-headquarters-postal-code").value = "70372";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
	    document.getElementById("selection-5299000W0HB7SX4P4882").onclick = function changeEdit(event) {
			document.getElementById("company-name-title").innerHTML = "Mercedes-Benz AG";

			// Registration
			const regDate = convertDate("Thu Sep 27 00:00:00 CEST 2018");
			const lastUpdateDate = convertDate("Thu Aug 20 00:00:00 CEST 2020");
			const nextRenewalDate = convertDate("Mon Sep 27 00:00:00 CEST 2021");
			
			document.getElementById("edit-initial-registration-date").value = regDate;
			document.getElementById("edit-last-update-date").value = lastUpdateDate;
			document.getElementById("edit-registration-status").value = "ISSUED";
			document.getElementById("edit-next-renewal-date").value = nextRenewalDate;

			// Links
			const linksLei = "invalid";
			const relationshipLei = "invalid";
			document.getElementById("links-lei-record").value = (linksLei === 'invalid' ? '' : linksLei);
			document.getElementById("links-relationship-record").value = (relationshipLei === 'invalid' ? '' : relationshipLei);

			// Lei
			document.getElementById("edit-lei").value = "5299000W0HB7SX4P4882";

			// Company left
			document.getElementById("company-left-legal-name").value = "Mercedes-Benz AG";
			document.getElementById("company-left-legal-language").value = "";
			document.getElementById("company-left-status").value = "ACTIVE";

			document.getElementById("company-left-legal-address-number").value = "";
			document.getElementById("company-left-legal-address-number-within-building").value = "";
			document.getElementById("company-left-legal-mail-routing").value = "";
			document.getElementById("company-left-legal-city").value = "Stuttgart";
			document.getElementById("company-left-legal-region").value = "Stuttgart";
			document.getElementById("company-left-legal-postal-code").value = "70372";

			document.getElementById("company-left-jurisdiction").value = "DE";

			// Company right
			document.getElementById("company-right-associated-entity-lei").value = "";
			document.getElementById("company-right-associated-entity-name").value = "";

			document.getElementById("company-right-successor-entity-lei").value = "";
			document.getElementById("company-right-successor-entity-name").value = "";

			document.getElementById("company-right-headquarters-address-number").value = "";
			document.getElementById("company-right-headquarters-address-number-within-building").value = "";
			document.getElementById("company-right-headquarters-mail-routing").value = "";
			document.getElementById("company-right-headquarters-city").value = "Stuttgart";
			document.getElementById("company-right-headquarters-region").value = "Stuttgart";
			document.getElementById("company-right-headquarters-postal-code").value = "70372";
	    }

		function convertDate(date) {
			const parts = date.split(" ");

			const months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
			const year = parts[parts.length - 1];
 			
			return year + "-" + months[parts[1]] + "-" + parts[2];
		}

		document.getElementById("submit-form").onclick = function submitForm(event) {
			console.log("saving: ", event);

			return false;
		}
}
